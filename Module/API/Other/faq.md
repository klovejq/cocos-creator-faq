## Cocos Creator FAQ
### Other - 其他

---
### Q1 
#### Question: Cocos Creator 3.x 怎么定义全局变量
Answer: 如果是 TS(typescript)脚本，你可以使用 window.aaa = aaa; 定义。 使用的时候，使用 aaa.x 即可(如果提示语法报错，那么可以使用 //@ts-ingnore 忽略报错提示)。    
如果是 JS(javascript)脚本，你可以使用 window.aaa = aaa; 定义。选择导入为插件，选择插件类型为 顶层。使用的时候，使用 aaa.x 即可(如果提示语法报错，那么可以使用 //@ts-ingnore 忽略报错提示)。
也可以使用 webpacker 把 TS 脚本打包成 JS 插件来使用。

---
### Q2
#### Question: Cocos Creator 3.x 版本没有 readPixels 接口
Answer: Cocos Creator 3.0 至 3.3.2 版本没有 readPixels 接口，在 3.4.0 版本会提供。    
@panda https://forum.cocos.org/t/topic/124094/305  3.0 至 3.3.2 版本需要实现 readPixels 接口，可以考虑合并 PR
https://github.com/cocos-creator/engine-native/pull/3710/files    
https://github.com/cocos-creator/engine/pull/8984    
https://github.com/cocos-creator/engine-native/pull/3736/files    

---
### Q3
#### Question: 调用 releaseAll 释放 bundle 内资源报错
R: 调用 releaseAll 释放 bundle 内资源报错，报错信息 Uncaught TypeError: Cannot read properties of null (reading 'getFGXTexture')

Answer: 这个报错是因为正在使用的纹理资源给释放了。

---
### Q4
#### Question: 第一次热更新会更新下载全部的文件, 之后热更新没问题
R: 热更新，第一次会下载全部的文件，之后再热更新，就会热更新更新的文件，是不是什么东西配置有问题？    

Answer: 首先排查是否是你首版本的 manifest 文件数据有问题，导致需要更新所有资源。检查一下，检查热更新的时候，是否加载 version.manifest 和 project.manifest 的位置异常导致的问题。