## Cocos Creator FAQ
### Other - 其他

### FAQ
| 编号 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | [Cocos Creator 3.x 怎么定义全局变量](./faq.md#q1) | FAQ问答 |
| 2 | [Cocos Creator 3.x 版本没有 readPixels 接口](./faq.md#q2) | FAQ问答 |
| 3 | [调用 releaseAll 释放 bundle 内资源报错](./faq.md#q3) | FAQ问答 |
| 4 | [第一次热更新会更新下载全部的文件, 之后热更新没问题](./faq.md#q4) | FAQ问答 |