## Cocos Creator FAQ
### Resource - 资源管理

### Usage
| 编号 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | [加载 resources 预制体](./usage.md#u1) | 基础使用 |
| 2 | [加载 resources 动画片段](./usage.md#u2) | 基础使用 |
| 3 | [加载 resources 图片](./usage.md#u3) | 基础使用 |
| 4 | [加载 resources 纹理](./usage.md#u4) | 基础使用 |