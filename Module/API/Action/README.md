## Cocos Creator FAQ
### Action - 动作

### Usage
| 编号 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | [立即显示](./usage.md#u1) | 基础使用 |
| 2 | [立即隐藏](./usage.md#u2) | 基础使用 |
| 3 | [显隐切换](./usage.md#u3) | 基础使用 |
| 4 | [渐显效果](./usage.md#u4) | 基础使用 |
| 5 | [渐隐效果](./usage.md#u5) | 基础使用 |