### Particle2d / 2D 粒子

---
### 1
CocosCreator 3.3.1 版本，使用 2d 粒子(粒子类型位置选择 FREE)在原生端闪屏, web 端显示正常?

A: 这个是由于引擎的 BUG 导致的，可以通过修改引擎代码修复。修改 particle-system-2d-assembler.ts文件的 ParticleAssembler 的 fillBuffers 接口。(particle-system-2d-assembler.ts 路径为: engine/cocos/particle-2d/particle-system-2d-assembler)
```
// ...
if (!isRecreate) {
    buffer = renderer.currBufferBatch!;
    indicesOffset = 0;
    vertexOffset = 0; // 新增，修复BUG
    vertexId = 0;
}
// ...
```
    修改后，重新编译引擎。