## Cocos Creator FAQ
### Sprite / 图片

### FAQ
| 编号 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | [3.x 使用大平面作为地形移动时边界会有闪烁](./faq.md#q1) | FAQ问答 |
| 2 | [3.x 中纹理的过滤模式 Point 对应](./faq.md#q2) | FAQ问答 |
| 3 | [纹理设置只显示指定的矩形区域](./faq.md#q3) | FAQ问答 |
| 4 | [shader 实现的水纹不连续](./faq.md#q4) | FAQ问答 |
| 5 | [截图下载下来的图片不同机型不一致](./faq.md#q5) | FAQ问答 |