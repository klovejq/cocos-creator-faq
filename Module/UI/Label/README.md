## Cocos Creator FAQ
### Label / 文本

### Customize
| 编号 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | [支持越南语换行](./customize.md#c1) | 支持2.4.x |
| 2 | [支持泰语换行](./customize.md#c2) | 支持2.4.x |
| 3 | [支持印地语换行](./customize.md#c3) | 支持2.4.x |