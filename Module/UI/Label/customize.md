## Cocos Creator FAQ
### Label / 文本

---
### C1
#### 支持越南语换行
[方案](./SupportVietnameseLineBreak)

---
### C2
#### 支持泰语换行
[方案](./SupportThaiLineBreak)

---
### C3
#### 支持印地语换行
[方案](./SupportDevanagariLineBreak)