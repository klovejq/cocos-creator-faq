## Cocos Creator FAQ

### 简介
支持越南语换行

### 测试
Môn Khách kiếm tiền càng cao, thì tiêu hao càng ít Bạc khi mậu dịch thành phố. Khi không đủ Bạc sẽ không thể tiến hành mậu dịch

Khi đàm phán với mục tiêu có thể sử dụng Bạc hoặc Thịt Khô để cổ vũ, cổ vũ có thể tạm thời tăng trị số kiếm tiền.

Trong quá trình mậu dịch sẽ có xác suất gặp sự kiện ngẫu nhiên, sau khi hoàn thành sự kiện là nhận được lượng thưởng lớn, sau khi thân phận đạt 30 là có thể sử dụng tính năng xử lý nhanh.

Cạnh Tranh Hương LiệuLiệu

### 使用
<details>
<summary> 3.x 使用方式 </summary>

  把 text-utils.ts 替换到自定义引擎engine下，替换覆盖 engine/cocos/2d/utils/text-utils.ts

  替换前
  ![image](../../../pic/image/202203/2022030221.jpeg)

  替换后
  ![image](../../../pic/image/202203/2022030222.jpeg)
</details>

<details>
<summary> 2.x 使用方式 </summary>

  2.4.x版本，把 SolvingVietnameseLineFeeds.js 脚本放入到项目下即可
</details>