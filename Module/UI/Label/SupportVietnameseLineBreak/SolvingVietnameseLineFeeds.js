
/**
 * Listening for events after engine initialization
 * @author muxiandong 2021-11-19
 */
 cc.game.once(cc.game.EVENT_ENGINE_INITED, () => {
    // @ts-ignore
    cc.textUtils.label_wordRex = /([a-zA-Z0-9ÄÖÜäöüßéèçàùêâîôûа-яА-ЯЁё\u00C0-\u00FF\u1EA0-\u1EFF\u0180-\u01BF]+|\S)/;
    // @ts-ignore
    cc.textUtils.label_lastWordRex = /([a-zA-Z0-9ÄÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁё\u00C0-\u00FF\u1EA0-\u1EFF\u0180-\u01BF]+|\S)$/;
    // @ts-ignore
    cc.textUtils.label_lastEnglish = /[a-zA-Z0-9ÄÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁё\u00C0-\u00FF\u1EA0-\u1EFF\u0180-\u01BF]+$/;
    // @ts-ignore
    cc.textUtils.label_firstEnglish = /^[a-zA-Z0-9ÄÖÜäöüßéèçàùêâîôûаíìÍÌïÁÀáàÉÈÒÓòóŐőÙÚŰúűñÑæÆœŒÃÂãÔõěščřžýáíéóúůťďňĚŠČŘŽÁÍÉÓÚŤżźśóńłęćąŻŹŚÓŃŁĘĆĄ-яА-ЯЁё\u00C0-\u00FF\u1EA0-\u1EFF\u0180-\u01BF]/;
});