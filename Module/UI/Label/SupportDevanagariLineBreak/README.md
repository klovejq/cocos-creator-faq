## Cocos Creator FAQ

### 简介
支持印地语(天城文书)换行

### 测试
अपग्रेड होने की सामग्री

### 使用
<details>
<summary> 3.x 使用方式 </summary>

  把 text-utils.ts 替换到自定义引擎engine下，替换覆盖 engine/cocos/2d/utils/text-utils.ts

  替换前
  ![image](../../../pic/image/202203/2022030201.jpeg)

  替换后
  ![image](../../../pic/image/202203/2022030202.jpeg)
</details>

<details>
<summary> 2.x 使用方式 </summary>

  2.4.x版本，把 SolvingHindiLineFeeds.js 脚本放入到项目下即可
</details>