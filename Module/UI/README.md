## 用户交互 UI

* 简介
    
    UI，会包含常见用于用户交互的组件，如按钮、文本、富文本、滚动视图、图片等。

| 编号 | 分类 | 备注 |
| :---: | :---: | :---: |
| 1 | [Sprite](./Sprite) | 图片 |
| 2 | [RichText](./RichText) | 富文本 |
| 3 | [Label](./Label) | 文本 |
| 4 | [Scrollview](./Scrollview) | 滚动视图 |
| 5 | [Button](./Button) | 按钮 |