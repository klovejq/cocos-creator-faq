
import { _decorator, Component, Node, systemEvent, SystemEvent,
    find, Button, EventHandler, Vec3 } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('test')
export class test extends Component {

    onLoad () {
        this.enableCustomButtons(false);
    }

    start () {
        find('Canvas/Button').eventProcessor.touchListener.setSwallowTouches(false);

        systemEvent.on(SystemEvent.EventType.TOUCH_MOVE, (event) => {
            console.error('SYSTEM TOUCH MOVE');
        })
        systemEvent.on(SystemEvent.EventType.TOUCH_START, (event) => {
            console.error('SYSTEM TOUCH START');
        })
        systemEvent.on(SystemEvent.EventType.TOUCH_END, (event) => {
            console.error('SYSTEM TOUCH END');
        })
        systemEvent.on(SystemEvent.EventType.TOUCH_CANCEL, (event) => {
            console.error('SYSTEM TOUCH CANCEL');
        })

        find('Canvas/Button')!.on(Node.EventType.TOUCH_START, this.onButtonTouchBegin, this);
        find('Canvas/Button')!.on(Node.EventType.TOUCH_END, this.onButtonTouchEnd, this);
        find('Canvas/Button')!.on(Node.EventType.TOUCH_MOVE, this.onButtonTouchMove, this);
        find('Canvas/Button')!.on(Node.EventType.TOUCH_CANCEL, this.onButtonTouchCancel, this);
    }

    enableCustomButtons (enable: boolean) {

        // @ts-ignore
        Button.prototype._onTouchEnded = (event: EventTouch) => {
            console.log("_onTouchEnded");
            let self = (event.target as Node)!.getComponent(Button)!;
            if (!self.interactable || !self.enabledInHierarchy) return;

            if ((self as any)._pressed) {
                EventHandler.emitEvents(self.clickEvents, event);
                this.node.emit('click', this);
            }
            (self as any)._pressed = false;
            (self as any)._updateState();
            if (event) {
                event.propagationStopped = enable;
            }
        }

        // @ts-ignore
        Button.prototype._onTouchBegan = (event: EventTouch) => {
            console.log("_onTouchBegan")
            let self = (event.target as Node)!.getComponent(Button)!;

            if (!self.interactable || !self.enabledInHierarchy) { return; }
    
            (self as any)._pressed = true;
            (self as any)._updateState();
            if (event) {
                event.propagationStopped = enable;
            }
        }

        // @ts-ignore
        Button.prototype._onTouchMove = (event: EventTouch) => {
            console.log("_onTouchMove")
            let self = (event.target as Node)!.getComponent(Button)!;

            if (!self.interactable || !self.enabledInHierarchy || !(self as any)._pressed) { return; }
    
            if (!event) {
                return;
            }
    
            const touch = (event).touch;
            if (!touch) {
                return;
            }
    
            const hit = this.node._uiProps.uiTransformComp!.isHit(touch.getUILocation());
    
            if ((self as any)._transition === 3 && self.target && (self as any)._originalScale) {
                if (hit) {
                    Vec3.copy((self as any)._fromScale, (self as any)._originalScale);
                    Vec3.multiplyScalar((self as any)._toScale, (self as any)._originalScale, (self as any)._zoomScale);
                    (self as any)._transitionFinished = false;
                } else {
                    (self as any)._time = 0;
                    (self as any)._transitionFinished = true;
                    self.target.setScale((self as any)._originalScale);
                }
            } else {
                let state;
                if (hit) {
                    state = 'pressed';
                } else {
                    state = 'noprmal';
                }
                (self as any)._applyTransition(state);
            }
    
            if (event) {
                event.propagationStopped = enable;
            }
        }

        // @ts-ignore
        Button.prototype._onTouchCancel = (event: EventTouch) => {
            console.log("_onTouchCancel")
            let self = (event.target as Node)!.getComponent(Button)!;

            if (!self.interactable || !self.enabledInHierarchy) { return; }
    
            (self as any)._pressed = true;
            (self as any)._updateState();
        }

    }

    onButtonTouchBegin (evt) {
        console.warn('BUTTON TOUCH START');
    }

    onButtonTouchEnd (evt) {
        console.warn('BUTTON TOUCH END');
    }

    onButtonTouchMove (evt) {
        console.warn('BUTTON TOUCH MOVE');
    }

    onButtonTouchCancel (evt) {
        console.warn('BUTTON TOUCH CANCEL');
    }
}


