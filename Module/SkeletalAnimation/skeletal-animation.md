### SkeletalAnimation / 骨骼动画

---
### 1
CocosCreator 3.x，SkeletalAnimation 的触摸事件如何实现?3D模型的触摸事件。systemEvent被废弃了，用什么代替。

A: 3D模型的触摸事件。用射线检测实现 https://gitee.com/yeshao2069/cocos-creator-how-to-use/tree/v3.0.0/Physics/PhysicsRaycast     
systemEvent 现在用input代替 https://docs.cocos.com/creator/manual/zh/engine/event/event-input.html