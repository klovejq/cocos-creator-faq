## Cocos Creator FAQ
### Optimization / 优化

### Usage
| 编号 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | [项目优化 / 内存优化](./usage.md#u1) | 进阶优化 |
| 2 | [项目优化 / 内存优化&性能优化](./usage.md#u2) | 进阶优化 |
| 3 | [项目优化 / 资源管理](./usage.md#u3) | 进阶优化 |
| 4 | [编辑器卡顿优化 / 2.x](./usage.md#u4) | 进阶优化 |

### FAQ
| 编号 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | [resources.load 控制并发加载](./faq.md#q1) | FAQ问答 |