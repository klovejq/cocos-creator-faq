### Spine

### FAQ
| 编号 | 版本 | 子项 | 备注 |
| :---: | :---: | :---: | :---: |
| 1 | 2.4.7 | [spine / 闪白效果使用**PVR**显示异常](./spine.md#1) | FAQ问答 |
| 2 | 2.4.7 | [spine / **PVR**设置预乘显示异常](./spine.md#2) | FAQ问答 |
| 5 | 2.x / 3.x | [spine / **Spine合批**怎么使用](./spine.md#5) | 基础使用 |