### Spine 动画

---
### 1
Cocos Creator 2.4.7 编写 spine 的闪白 shader 效果，web 预览正常(png)，使用 pvr 显示异常?

A: Shader 的 effect 文件中，宏定义 'CC_USE_ALPHA_ATLAS_TEXTURE' 需要修改为 'CC_USE_ALPHA_texture'

---
### 2
CocosCreator 2.4.7，spine动画，设置预乘(在编辑器勾选 Premultiplied Alpha)，设置 pvr 后，编辑器上显示异常。

A:  可以检查下，编辑器上的预乘选项，需要 spine 动画纹理导出的时候就勾选了预乘才能勾选。编辑器上的预乘选项，对 pvr 图片是无效的。Spine 动画的资源如果是透明的，需要导出的时候设置导出透明度即可。

---
### 5
CocosCreator 编辑器上，Spine 合批怎么使用？

A: 
2.x版本
```typescript
@property(sp.Skeleton)
sp0 !: sp.Skeleton;
@property(sp.Skeleton)
sp1 !: sp.Skeleton;
@property(sp.Skeleton)
sp2 !: sp.Skeleton;

this.sp0.enableBatch = true;
this.sp1.enableBatch = true;
this.sp2.enableBatch = true;
```
3.x版本
> 3.x版本(截止目前最新版本3.5.0)，Spine目前不支持跨节点合批，开启spine.enableBatch会导致spine无法正常渲染。因为底层渲染数据提交逻辑发生变化了。后续会考虑使用 instancing 合批。https://forum.cocos.org/t/topic/109133/3