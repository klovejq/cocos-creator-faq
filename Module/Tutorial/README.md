## Cocos Creator FAQ

### Tutorial / 基础教程


<details>
 <summary>DCC  |  What is DCC ?</summary>
  Cocos Creator 中的 DCC 指的是 Digital Content Creation (数字内容制作)工具，简称 DCC 工具。    
  
  目前常用的 DCC 工具如 3ds Max、Maya、Blender、Spine、DragonBones 等等。
</details>

<details>
 <summary>CCD  |  What is CCD ?</summary>
  Cocos Creator 中的 CCD 指的是 Continuous Collision Detection (连续碰撞检测)。    
  
  是一种用于避免高速运动的物体在离散运动时出现穿透现象而导致碰撞数据不精确的技术，可用于实现类似高速运动的子弹与物体发生碰撞时，不会直接穿透物体。
</details>