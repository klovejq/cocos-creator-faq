### DragonBones / 龙骨动画

--- 
### 1
CocosCreator 3.3.1，贴图没有压缩过，在使用龙骨动画时发现贴图的连接处会有黑色的缝隙。    

A: 图片没压缩过的话，可以对贴图本身做一下预乘处理试试。可以参考 https://forum.cocos.org/t/topic/124355