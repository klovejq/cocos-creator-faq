## Cocos Creator FAQ
### Physics3d - 3D 物理

--- 
### Q1
#### Question: 物理引擎可以做成无渲染的(独立), 放到服务器上?
#### Version: Cocos Creator All
- 咨询: 物理引擎可以做成无渲染的(独立), 放到服务器上吗
- 专家门诊: 物理引擎本身并不带渲染，可以独立，可以放到服务器上。

---
### Q2
#### Question: 是否提供定点数的物理引擎?
#### Version: Cocos Creator All
- 咨询: 是否提供定点数的物理引擎
- 专家门诊: 目前已知的常用的物理引擎都不是定点数计算的 (是浮点数计算)。