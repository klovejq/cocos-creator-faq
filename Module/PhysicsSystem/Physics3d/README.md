## Cocos Creator FAQ
### Physics3d - 3D 物理

### FAQ
| 编号 | 子项 | 备注 |
| :---: | :---: | :---: |
| 1 | [独立物理引擎](./faq.md#q1) | FAQ问答 |
| 2 | [定点数物理引擎](./faq.md#q2) | FAQ问答 |