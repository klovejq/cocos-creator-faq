## Cocos Creator FAQ
### Font / 位图字体工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| BMFont | http://www.angelcode.com/products/bmfont | 免费 | Windows |
| Glyph Designer | http://glyphdesigner.71squared.com/ | 付费 | Mac |
| Hiero | https://github.com/libgdx/libgdx/wiki/Hiero | 免费 | Windows/Mac |
| BMFontEditor | https://joshuahxh.com/bitmap-font-editor/ | 免费 | online |