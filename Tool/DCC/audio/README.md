## Cocos Creator FAQ
### Audio / 声音特效编辑工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| bfxr | http://www.bfxr.net/ | 免费 | Windows/Mac/Online |
| Labchirp | http://labbed.net/software.php?id=labchirp | 免费 | Windows |
| Sound Studio | http://felttip.com/ss/ | 免费 |  Mac |