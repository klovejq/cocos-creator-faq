## Cocos Creator FAQ
### Music / 背景音乐编辑工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| GarageBand | http://www.apple.com/cn/mac/garageband/ | 付费 | Mac |
| Reaper | https://www.cockos.com/reaper/download.php | 付费 | Windows/Mac |
| Ardour | http://ardour.org/ | 免费 | Windows/Mac |