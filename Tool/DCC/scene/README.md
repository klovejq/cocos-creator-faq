## Cocos Creator FAQ
### Scene / 场景编辑工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| CocosBuilder | https://github.com/cocos2d/CocosBuilder | 开源 | Mac |
| SpriteBuilder | http://www.spritebuilder.com/ | 开源 | Mac |
| Cocoshop | https://github.com/andrew0/cocoshop | 开源 | Mac |
| FairyGUI | http://www.fairygui.com/ | 开源 | Windows/Mac |