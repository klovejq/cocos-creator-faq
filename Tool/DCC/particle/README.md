## Cocos Creator FAQ
### Particle / 粒子编辑工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| ParticleCreator | https://itunes.apple.com/us/app/particle-creator-for-cocos2d/id564925232?mt=8 | 免费 | iOS |
| Particle Designer | http://particledesigner.71squared.com/ | 收费 | Mac |
| Particle Universe | http://www.ogre3d.org/tikiwiki/Particle+Universe+plugin | 免费 | Windows |
| Particle2dx | http://www.effecthub.com/particle2dx | 免费 | Online |