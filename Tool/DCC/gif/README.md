## Cocos Creator FAQ
### GIF / GIF动画帧导出工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| UleadGifAnimator | http://www.crsky.com/soft/4010.html | 免费 | Windows |
| ShoeBox | http://renderhjs.net/shoebox/ | 免费 | Windows/Mac |
| Kap | https://github.com/wulkano/kap | 免费 | Mac |
| Bandicam | https://www.bandicam.cn/downloads/ | 付费 | Windows |
| ScreenToGif | https://www.screentogif.com/downloads | 免费 | Windows |
| GifCam | http://blog.bahraniapps.com/gifcam/#download | 免费 | Windows |
| GIF压缩 | https://docsmall.com/gif-compress | 免费 | online(支持25M以下GIF压缩) |