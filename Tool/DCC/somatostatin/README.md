## Cocos Creator FAQ
### Somatostatin / 体素编辑工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| MagicaVoxel | https://ephtracy.github.io/ | 免费 | Windows/Mac |
| QUBICLE | https://www.getqubicle.com/index.html | 付费 | Windows/Mac |
| VoxelShop | https://github.com/simlu/voxelshop/releases | 免费 | Windows/Mac |