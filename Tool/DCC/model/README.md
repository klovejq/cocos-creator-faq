## Cocos Creator FAQ
### Model / 3D模型工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| FBX-Converter | https://www.autodesk.com/developer-network/platform-technologies/fbx-converter-archives | 免费 | Windows/Mac |
| mixamo | https://www.mixamo.com/#/?page=1&type=Character | online |