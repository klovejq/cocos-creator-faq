## Cocos Creator FAQ
### Tieldmap / 瓦片地图编辑工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| iTileMaps | https://www.klemix.com/ | 免费 | iOS |
| Tiled Map Editor | http://www.mapeditor.org/ | 开源 | Windows/Mac |
| Tiled Map Editor(History) | https://sourceforge.net/projects/tiled/files/ | 开源 | Windows/Mac |