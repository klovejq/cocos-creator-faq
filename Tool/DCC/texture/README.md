## Cocos Creator FAQ
### Texture / 纹理图集工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| DarkFunction Editor | http://darkfunction.com/editor/ | 免费 | Windows/Mac |
| TexturePacker | https://www.codeandweb.com/texturepacker | 付费 | Windows/Mac |
| SpriteUV | http://www.spriteuv.com/ | 免费 | Windows |
| TinyPNG | https://tinypng.com/ | 免费/付费 | Online |
| PngQuant | https://pngquant.org/ | 免费 | Online |
| ShoeBox | http://renderhjs.net/shoebox/ | 免费 | Windows/Mac |
| BigShear | http://renderhjs.net/shoebox/ | 免费 | Windows/Mac |
| SVG ICON | https://game-icons.net/ | 免费 | Online |
| PNG 转 JPG | https://png2jpg.com/zh/ | 免费 | online |
| WEBP 转 JPG | https://www.aconvert.com/cn/image/webp-to-jpg/ | 免费 | online(支持JPG、PNG、SVG、TIFF、GIF、BMP、PS、PSD、WEBP、TGA、DDS、EXR、PNM、HEIC格式互转) |
| JPG、PNG压缩 | https://docsmall.com/image-compress | 免费 | online(支持25M以下JPG、PNG压缩) |
| PDF压缩 | https://docsmall.com/pdf-compress | 免费 | online(支持200M以下PDF压缩) |
| PDF合并 | https://docsmall.com/pdf-merge | 免费 | online(支持500M以下PDF合并) |
| PDF分割 | https://docsmall.com/pdf-split | 免费 | online(支持500M以下、1000页内PDF分割) |