## Cocos Creator FAQ
### Physics / 物理编辑工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| PhysicsBench | https://sourceforge.net/projects/physicsbench/ | 免费 | Windows/Mac |
| PhysicsEditor | https://www.codeandweb.com/physicseditor | 付费 | Windows/Mac |
| VertexHelper | https://github.com/jfahrenkrug/VertexHelper | 开源 | Mac |