## Cocos Creator FAQ
### Anim / 骨骼动画编辑工具

| 工具名 | 下载地址 | 授权 | 支持平台 |
| :---: | :---: | :---: | :---: |
| Spine | http://zh.esotericsoftware.com/ | 收费 | Windows/Mac |
| Dragonbones | http://dragonbones.github.io/ | 开源 | Windows/Mac |
| Spriter | http://www.brashmonkey.com/ | 付费 | Windows/Mac |