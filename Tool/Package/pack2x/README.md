## Cocos Creator FAQ

### Package Download Link / 安装包下载地址(在线地址)

#### 使用方式： 如果直接店家下载无效的话(浏览器访问被限制)，可以复制下载链接到浏览器新的 TAB 打开，然后即可下载。

### Cocos Creator v2 下载
| 文件名 | 下载链接 | 百度网盘 |
| :---: | :---: | :---: |
| CocosCreator_v2.3.1_win | https://cocos2d-x.org/filedown/CocosCreator_v2.3.1_win | 链接: https://pan.baidu.com/s/1yzRYgfDMJ5HH2l0T1ILh3w 提取码: 4vos |
| CocosCreator_v2.3.1_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.3.1_mac | 链接: https://pan.baidu.com/s/1BFWsztRygBXEO5fINhNYLA 提取码: 3oqn |
| CocosCreator_v2.3.0_win | https://cocos2d-x.org/filedown/CocosCreator_v2.3.0_win | 链接: https://pan.baidu.com/s/1z2FQA2Sjf6IRAK0h1aI0BQ 提取码: e0do |
| CocosCreator_v2.3.0_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.3.0_mac | 链接: https://pan.baidu.com/s/1c5kqG1N06XqzUME-MIWAkg 提取码: 9gk1 |
| CocosCreator_v2.2.2_win | https://cocos2d-x.org/filedown/CocosCreator_v2.2.2_win | 链接: https://pan.baidu.com/s/1579wsv7-WDA2SME4HhdwWg 提取码: em9o |
| CocosCreator_v2.2.2_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.2.2_mac | 链接: https://pan.baidu.com/s/168ThGnzOWboI4TKk8bz0Gw 提取码: 3evr |
| CocosCreator_v2.2.1_win | https://cocos2d-x.org/filedown/CocosCreator_v2.2.1_win | 链接: https://pan.baidu.com/s/1YYQAQkKV3hjvsDtUYvhtLA 提取码: 0h3i |
| CocosCreator_v2.2.1_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.2.1_mac | 链接: https://pan.baidu.com/s/1u1BmPb9HuMHW2iNkqvIEcA 提取码: 565w |
| CocosCreator_v2.2.0_win | https://cocos2d-x.org/filedown/CocosCreator_v2.2.0_win | 链接: https://pan.baidu.com/s/1H2DepWC6QnkZbiTUF6-ZOQ 提取码: kdia |
| CocosCreator_v2.2.0_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.2.0_mac | 链接: https://pan.baidu.com/s/1AAIivsQila8StfeNYyHpMA 提取码: ints |
| CocosCreator_v2.1.4_win | https://cocos2d-x.org/filedown/CocosCreator_v2.1.4_win | 链接: https://pan.baidu.com/s/1-mgQ-QxbMUacIs-p-Cficw 提取码: td6b |
| CocosCreator_v2.1.4_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.1.4_mac | 链接: https://pan.baidu.com/s/1umrQ5DVF0rI9o5RdNJJD-A 提取码: 88kb |
| CocosCreator_v2.1.3_win | https://cocos2d-x.org/filedown/CocosCreator_v2.1.3_win | 链接: https://pan.baidu.com/s/1quamqrw5_o6v94jhb09cpQ 提取码: kpma |
| CocosCreator_v2.1.3_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.1.3_mac | 链接: https://pan.baidu.com/s/1huF00TWPDF34SCg2jIebOA 提取码: 50dk |
| CocosCreator_v2.1.2_win | https://cocos2d-x.org/filedown/CocosCreator_v2.1.2_win | 链接: https://pan.baidu.com/s/1OHJaEePYVs0liC-QA-B3tQ 提取码: srm5 |
| CocosCreator_v2.1.2_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.1.2_mac | 链接: https://pan.baidu.com/s/1MxkJ7KILC0z7Dm_EB_9LPw 提取码: cpnv |
| CocosCreator_v2.1.1_win | https://cocos2d-x.org/filedown/CocosCreator_v2.1.1_win | 链接: https://pan.baidu.com/s/1u4-v28hTC58usizuZA9CJg 提取码: 10th |
| CocosCreator_v2.1.1_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.1.1_mac | 链接: https://pan.baidu.com/s/1lclIJ7q-pbaNrX3uJoQHbw 提取码: 7r3t |
| CocosCreator_v2.1.0_win | http://download.cocos.com/CocosCreator/v2.1.0/CocosCreator_v2.1.0_20181127_win.7z | 链接: https://pan.baidu.com/s/1TUYQ0g5Q9uV6e5AtlNgspg 提取码: 7ild |
| CocosCreator_v2.1.0_mac | http://download.cocos.com/CocosCreator/v2.1.0/CocosCreator_v2.1.0_20181127_mac.dmg | 链接: https://pan.baidu.com/s/1JV3u9pvfmrjLkDGde066hw 提取码: 5e7b |
| CocosCreator_v2.0.10_win | https://cocos2d-x.org/filedown/CocosCreator_v2.0.10_win | 链接: https://pan.baidu.com/s/1X_koKgLbyQv6YOEAS-J_9w 提取码: kwf9 |
| CocosCreator_v2.0.10_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.0.10_mac | 链接: https://pan.baidu.com/s/10T0_ewD7tQ2vo__fZfQaHA 提取码: vaqb |
| CocosCreator_v2.0.9_win | https://cocos2d-x.org/filedown/CocosCreator_v2.0.9_win | 链接: https://pan.baidu.com/s/1N4iC07RNzg1v6vlauLe_6g 提取码: f9eq |
| CocosCreator_v2.0.9_mac | https://cocos2d-x.org/filedown/CocosCreator_v2.0.9_mac | 链接: https://pan.baidu.com/s/1M_nYw8h3NIXRfRbOtt_szQ 提取码: lf00 |
| CocosCreator_v2.0.8_win | http://download.cocos.com/CocosCreator/v2.0.8/CocosCreator_v2.0.8_20190210_win.7z | 链接: https://pan.baidu.com/s/1dqBGGlJPYPb7nXh41mt5ZQ 提取码: 7dqv |
| CocosCreator_v2.0.8_mac | http://download.cocos.com/CocosCreator/v2.0.8/CocosCreator_v2.0.8_20190210_mac.dmg | 链接: https://pan.baidu.com/s/1_-p_9MHeSHGwhQgLlb53Dw 提取码: 6ljq |
| CocosCreator_v2.0.7_win | http://download.cocos.com/CocosCreator/v2.0.7/CocosCreator_v2.0.7_20190107_win.7z | 链接: https://pan.baidu.com/s/1kR4eHTfWjfiBTuJNSFhx7w 提取码: row3 |
| CocosCreator_v2.0.7_mac | http://download.cocos.com/CocosCreator/v2.0.7/CocosCreator_v2.0.7_20190107_mac.dmg | 链接: https://pan.baidu.com/s/1B-NUMS9DPlkePkrtkitdTQ 提取码: v7kl |
| CocosCreator_v2.0.6_win | http://download.cocos.com/CocosCreator/v2.0.6/CocosCreator_v2.0.6_20181213_win.7z | 链接: https://pan.baidu.com/s/19cCAuE-HmOH71FvDs7PG8w 提取码: nkq9 |
| CocosCreator_v2.0.6_mac | http://download.cocos.com/CocosCreator/v2.0.6/CocosCreator_v2.0.6_20181213_mac.dmg | 链接: https://pan.baidu.com/s/1Tee4ToStefna6xAi-AS__A 提取码: ju71 |
| CocosCreator_v2.0.5_win | http://download.cocos.com/CocosCreator/v2.0.5/CocosCreator_v2.0.5_2018110602_win.7z | 链接: https://pan.baidu.com/s/1kHR0y2QF-3KVdEM4kY3Z7g 提取码: dc19 |
| CocosCreator_v2.0.5_mac | http://download.cocos.com/CocosCreator/v2.0.5/CocosCreator_v2.0.5_2018110602_mac.dmg | 链接: https://pan.baidu.com/s/1u49Pa6An30-EZw9PafnxMA 提取码: t2fd |
| CocosCreator_v2.0.4_win | http://download.cocos.com/CocosCreator/v2.0.4/CocosCreator_v2.0.4_20181017_win.7z | 链接: https://pan.baidu.com/s/19CTgLtR8LItRZvafvT-qpA 提取码: mrd2 |
| CocosCreator_v2.0.4_mac | http://download.cocos.com/CocosCreator/v2.0.4/CocosCreator_v2.0.4_20181017_mac.dmg | 链接: https://pan.baidu.com/s/1KljT_yHWPO2SubwrM7E7Eg 提取码: o2ue |
| CocosCreator_v2.0.2_win | http://download.cocos.com/CocosCreator/v2.0.2/CocosCreator_v2.0.2_20180920_win.7z | 链接: https://pan.baidu.com/s/1-gu6kx9GZItL_hIuR5RNVw 提取码: hpfp |
| CocosCreator_v2.0.2_mac | http://download.cocos.com/CocosCreator/v2.0.2/CocosCreator_v2.0.2_20180920_mac.dmg | 链接: https://pan.baidu.com/s/1O8iO5NSW8itgkw53yk8gzA 提取码: i2f8 |
| CocosCreator_v2.0.1_win | http://download.cocos.com/CocosCreator/v2.0.1/CocosCreator_v2.0.1.7z | 链接: https://pan.baidu.com/s/1rPGOLkAZ5xKq5vkQzfFhLA 提取码: 3oi9 |
| CocosCreator_v2.0.1_mac | http://download.cocos.com/CocosCreator/v2.0.1/CocosCreator_v2.0.1_20180823.dmg | 链接: https://pan.baidu.com/s/1nDFokD9G3w-kHqLUI9KOJQ 提取码: uvht |
| CocosCreator_v2.0.0_win | http://download.cocos.com/CocosCreator/v2.0.0/CocosCreator_v2.0.0-p.1.7z | 链接: https://pan.baidu.com/s/1WLmuEeQnxGeQhtBrUtaPZg 提取码: dfs8 |
| CocosCreator_v2.0.0_mac | http://download.cocos.com/CocosCreator/v2.0.0/CocosCreator_v2.0.0-p.1_20180808.dmg | 链接: https://pan.baidu.com/s/1b6Q32Letw9n36T3pKP_D2Q 提取码: r8nv |