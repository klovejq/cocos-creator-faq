## 安装包（Creator 历史版本下载）

* 简介
    
    安装包，避免下载一些历史版本因为下载链接失效而无法下载的问题。

| 编号 | 分类 | 备注 |
| :---: | :---: | :---: |
| 1 | [CocosCreator 1.x下载](./pack1x) | 1.0.0 ~ 1.10.2 |
| 2 | [CocosCreator 2.x下载](./pack2x) | 2.0.0 ~ 2.3.1 |
| 3 | [CocosCreator 3D 1.x && 0.7.x版本](./packOld) | 3D 0.7.x & 3D 1.0.0 ~ 3D 1.2.0 |