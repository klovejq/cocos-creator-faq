## Cocos Creator FAQ

### Package Download Link / 安装包下载地址(在线地址)

#### 使用方式： 如果直接店家下载无效的话(浏览器访问被限制)，可以复制下载链接到浏览器新的 TAB 打开，然后即可下载。

### Cocos Creator v1 下载
| 文件名 | 下载链接 | 百度网盘 |
| :---: | :---: | :---: |
| CocosCreator_v1.10.2_win | http://download.cocos.com/CocosCreator/v1.10.2/CocosCreator_v1.10.2_20180930_win.7z | 链接: https://pan.baidu.com/s/1u0yLgetEBn1WEQrE_0LM9Q 提取码: 8nio |
| CocosCreator_v1.10.2_mac | http://download.cocos.com/CocosCreator/v1.10.2/CocosCreator_v1.10.2_20180930_mac.dmg | 链接: https://pan.baidu.com/s/1tlmrC427LFTNGV5B0cpF-Q 提取码: ar3s |
| CocosCreator_v1.10.1_win | http://download.cocos.com/CocosCreator/v1.10.1/CocosCreator_v1.10.1.7z | 链接: https://pan.baidu.com/s/19EuAL1IKgo-VtMFfvHJ3Eg 提取码: l5rm |
| CocosCreator_v1.10.1_mac | http://download.cocos.com/CocosCreator/v1.10.1/CocosCreator_v1.10.1_20180807.dmg | 链接: https://pan.baidu.com/s/19kchYWIhsxSev1MjLbn6sw 提取码: o9qk |
| CocosCreator_v1.10.0_win | http://download.cocos.com/CocosCreator/v1.10.0/CocosCreator_v1.10.0.7z | 链接: https://pan.baidu.com/s/1xj1TuvzWiyK72VNxzLLKJQ 提取码: tdak |
| CocosCreator_v1.10.0_mac | http://download.cocos.com/CocosCreator/v1.10.0/CocosCreator_v1.10.0_20180730.dmg | 链接: https://pan.baidu.com/s/1KpN6tzGXBjEz8YR8EAt56g 提取码: ce8o |
| CocosCreator_1.9.3_win | http://download.cocos.com/CocosCreator/v1.9.3/CocosCreator_v1.9.3.setup.7z | 链接: https://pan.baidu.com/s/1xl_kr9UxXFbKIUPBxRbNWA 提取码: jas4 |
| CocosCreator_v1.9.3_mac | http://download.cocos.com/CocosCreator/v1.9.3/CocosCreator_v1.9.3_20180704.dmg | 链接: https://pan.baidu.com/s/19gAHsrcOX-hinLsz00DMdQ 提取码: pg4c |
| CocosCreator_v1.9.2_win | http://download.cocos.com/CocosCreator/v1.9.2/CocosCreator_1.9.2_win_setup.exe.zip | 链接: https://pan.baidu.com/s/1MEdZ3e-S9L5UOwVAgba4-g 提取码: otld |
| CocosCreator_v1.9.2_mac | http://download.cocos.com/CocosCreator/v1.9.2/CocosCreator_v1.9.2_2018052903.dmg | 链接: https://pan.baidu.com/s/1RiHR68P257wZ8dV0hzTaJA 提取码: 25vo |
| CocosCreator_v1.9.1_win | http://download.cocos.com/CocosCreator/v1.9.1/CocosCreator_1.9.1_win.exe.zip | 链接: https://pan.baidu.com/s/12h7xuklKpFRhvVYGk_cZXg 提取码: fpo1 |
| CocosCreator_v1.9.1_mac | http://download.cocos.com/CocosCreator/v1.9.1/CocosCreator_v1.9.1_20180417.dmg | 链接: https://pan.baidu.com/s/1waQJs2xuCdRG9m7zP1CquA 提取码: e5hs |
| CocosCreator_v1.9.0_win | http://download.cocos.com/CocosCreator/v1.9.0/CocosCreator_v1.9.0_20180314_win.zip | 链接: https://pan.baidu.com/s/1e3jcJQPvGUb-KgdqxpTfHg 提取码: crum |
| CocosCreator_v1.9.0_mac | http://download.cocos.com/CocosCreator/v1.9.0/CocosCreator_v1.9.0_20180313.dmg | 链接: https://pan.baidu.com/s/1Q_KlUChFlhf7AnJjCHqxEg 提取码: jr2s |
| CocosCreator_v1.9.0-rc1_mac | http://www.cocos2d-x.org/filedown/CocosCreator_v1.9.0-rc1_mac | 链接: https://pan.baidu.com/s/1bsh1Qt-RLIb87rAYorI1Zw 提取码: cch5 |
| CocosCreator_v1.8.2-win | http://cdn.cocos2d-x.org/CocosCreator_v1.8.2_win_setup.exe.zip | 链接: https://pan.baidu.com/s/17wS88_ICl4sKBOKT73hCyg 提取码: wh7u |
| CocosCreator_v1.8.2-mac | http://download.cocos.com/CocosCreator/v1.8.2/CocosCreator_v1.8.2_20180305.dmg | 链接: https://pan.baidu.com/s/1_doOxMVijYeFzWtp9HUAkw 提取码: 32o2 |
| CocosCreator_v1.8.1-win | http://download.cocos.com/CocosCreator/v1.8.1/CocosCreator_v1.8.1_20180111_win_setup.exe | 链接: https://pan.baidu.com/s/1hxlbM1AN1FhpSwocMEj9pg 提取码: bj4a |
| CocosCreator_v1.8.1-mac | http://download.cocos.com/CocosCreator/v1.8.1/CocosCreator_v1.8.1_20180111.dmg | 链接: https://pan.baidu.com/s/1RwcRZd3Zij2EAATghns9JA 提取码: 26bs |
| CocosCreator_v1.8.0-win | http://download.cocos.com/CocosCreator/v1.8.0/CocosCreator_v1.8.0_20171227_win.zip | 链接: https://pan.baidu.com/s/1KTvp0jSdiNsPA1uCW6D1tA 提取码: unbo |
| CocosCreator_v1.8.0-mac | http://download.cocos.com/CocosCreator/v1.8.0/CocosCreator_v1.8.0_20171227.dmg | 链接: https://pan.baidu.com/s/12mm3krmFbNZkyrgXTFBwbQ 提取码: jq7i |
| CocosCreator_v1.7.2-win | http://download.cocos.com/CocosCreator/v1.7.2/CocosCreator_v1.7.2_20171225_win.zip | 链接: https://pan.baidu.com/s/1jPtHswtGAkGT4SkrZGco_Q 提取码: tsem |
| CocosCreator_v1.7.2-mac | http://download.cocos.com/CocosCreator/v1.7.2/CocosCreator_v1.7.2_20171225.dmg | 链接: https://pan.baidu.com/s/1FoEF9Er_oDKq6lI5lxrjrQ 提取码: rkjn |
| CocosCreator_v1.6.2-win | http://download.cocos.com/CocosCreator/v1.6.2/CocosCreator_v1.6.2_2017101301_setup.exe | 链接: https://pan.baidu.com/s/1g347yNErWRx41YW8MhgedA 提取码: vnj3 |
| CocosCreator_v1.6.2-mac | http://download.cocos.com/CocosCreator/v1.6.2/CocosCreator_v1.6.2_2017101301.dmg | 链接: https://pan.baidu.com/s/1n0a_4BYjpkjmXSXYTB4Z0w 提取码: 8qnf |
| CocosCreator_v1.6.1-win | http://download.cocos.com/CocosCreator/v1.6.1/CocosCreator_v1.6.1_2017082403_setup.exe | 链接: https://pan.baidu.com/s/19Ov0pr3lrmACDH7dijTvug 提取码: 5umj |
| CocosCreator_v1.6.1-mac | http://download.cocos.com/CocosCreator/v1.6.1/CocosCreator_v1.6.1_2017082403.dmg | 链接: https://pan.baidu.com/s/1d06rB2_RcZrlvHq__nheGw 提取码: 2tjd |
| CocosCreator_v1.6.0-win | http://download.cocos.com/CocosCreator/v1.6.0/CocosCreator_v1.6.0_2017081401_setup.exe | 链接: https://pan.baidu.com/s/1aCGknXupCmAjNbvEaBlRJA 提取码: 2a7b |
| CocosCreator_v1.6.0-mac | http://download.cocos.com/CocosCreator/v1.6.0/CocosCreator_v1.6.0_2017081401.dmg | 链接: https://pan.baidu.com/s/1A6mW4efHfD9TnF0qpB5PKA 提取码: 36n4 |
| CocosCreator_v1.5.2-win | http://download.cocos.com/CocosCreator/v1.5.2/CocosCreator_v1.5.2_2017070701_setup.exe | 链接: https://pan.baidu.com/s/1hpbCZm5e5cPc44Ps_IeXug 提取码: gb4g |
| CocosCreator_v1.5.2-mac | http://download.cocos.com/CocosCreator/v1.5.2/CocosCreator_v1.5.2_2017070701.dmg | 链接: https://pan.baidu.com/s/1iUbVG0IJHe02EiHTTC7cdQ 提取码: 34vu |
| CocosCreator_v1.5.1-win | http://download.cocos.com/CocosCreator/v1.5.1/CocosCreator_v1.5.1_2017061501_setup.exe | 链接: https://pan.baidu.com/s/1PBJsye58me1H7vYZEXCVhw 提取码: cmbq |
| CocosCreator_v1.5.1-mac | http://download.cocos.com/CocosCreator/v1.5.1/CocosCreator_v1.5.1_2017061501.dmg | 链接: https://pan.baidu.com/s/1-5KHj8JihsMPZ_sQxeHuWg 提取码: 1wp3 |
| CocosCreator_v1.5.0-win | http://download.cocos.com/CocosCreator/v1.5.0/CocosCreator_v1.5.0_2017051904_setup.exe | 链接: https://pan.baidu.com/s/1UUfDcTUn-XErQE9AhCMzuA 提取码: cjdr |
| CocosCreator_v1.5.0-mac | http://download.cocos.com/CocosCreator/v1.5.0/CocosCreator_v1.5.0_2017051903.dmg | 链接: https://pan.baidu.com/s/1td5EemH_gghvFrAu4N2lgA 提取码: ak5d |
| CocosCreator_v1.4.2-win | http://cocostudio.download.appget.cn/CocosCreator/v1.4.2/CocosCreator_v1.4.2_2017032901_setup.exe | 链接: https://pan.baidu.com/s/1y8c6Qz1wC_XvaDTvtvJTcQ 提取码: qi8l |
| CocosCreator_v1.4.2-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.4.2/CocosCreator_v1.4.2_2017032901.dmg | 链接: https://pan.baidu.com/s/1wCvwWUAJVvV_kxMZT8r4pg 提取码: 734c |
| CocosCreator_v1.4.1-win | http://cocostudio.download.appget.cn/CocosCreator/v1.4.1/CocosCreator_v1.4.1_2017031702_setup.exe | 链接: https://pan.baidu.com/s/19P7uCA15cQrzSENE0Pgacw 提取码: k8eh |
| CocosCreator_v1.4.1-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.4.1/CocosCreator_v1.4.1_2017031702.dmg | 链接: https://pan.baidu.com/s/1fwsek8SDNdWyPyoOXxHz2Q 提取码: 1ltl |
| CocosCreator_v1.4.0-win | http://cocostudio.download.appget.cn/CocosCreator/v1.4.0/CocosCreator_v1.4.0_2017021001_setup.exe | 链接: https://pan.baidu.com/s/1GSGNRAyk9KDs21HDtWFuFg 提取码: aw0s |
| CocosCreator_v1.4.0-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.4.0/CocosCreator_v1.4.0_2017021001.dmg | 链接: https://pan.baidu.com/s/1RwJxGl2wmQI2nW6CZzkHVQ 提取码: 032f |
| CocosCreator_v1.3.3-win | http://cocostudio.download.appget.cn/CocosCreator/v1.3.3/CocosCreator_v1.3.3_2016122003_setup.exe | 链接: https://pan.baidu.com/s/13X-p3hUSpPCWleWMfUcpgg 提取码: bs3g |
| CocosCreator_v1.3.3-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.3.3/CocosCreator_v1.3.3_2016122003.dmg | 链接: https://pan.baidu.com/s/1c7oxSFi3HPF94dnRD1JeCQ 提取码: 1jw5 |
| CocosCreator_v1.3.2-win | http://cocostudio.download.appget.cn/CocosCreator/v1.3.2/CocosCreator_v1.3.2_2016112101_setup.exe | 链接: https://pan.baidu.com/s/1QYoQ_g-OYCmUNNnPMiP_yQ 提取码: 8aes |
| CocosCreator_v1.3.2-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.3.2/CocosCreator_v1.3.2_2016112101.dmg | 链接: https://pan.baidu.com/s/1hYbSMwo8JeGlC24-czboow 提取码: q3re |
| CocosCreator_v1.3.1-win | http://cocostudio.download.appget.cn/CocosCreator/v1.3.1/CocosCreator_v1.3.1_2016110202_setup.exe | 链接: https://pan.baidu.com/s/1k5XrNelqgWRHwqecDrdy5g 提取码: rlh0 |
| CocosCreator_v1.3.1-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.3.1/CocosCreator_v1.3.1_2016110202.dmg | 链接: https://pan.baidu.com/s/1PiYBOVbNVTJO-NphMVv8gA 提取码: rb5v |
| CocosCreator_v1.3.0-win | http://cocostudio.download.appget.cn/CocosCreator/v1.3.0/CocosCreator_v1.3.0_2016102502_setup.exe | 链接: https://pan.baidu.com/s/1E2X4Tu2isaz-3fOt3eF-kQ 提取码: ngvu |
| CocosCreator_v1.3.0-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.3.0/CocosCreator_v1.3.0_2016102502.dmg | 链接: https://pan.baidu.com/s/1aP8e4bAOzjo5ci4Cwc2SQA 提取码: c6s5 |
| CocosCreator_v1.2.2-win | http://cocostudio.download.appget.cn/CocosCreator/v1.2.2/CocosCreator_v1.2.2_2016092001_setup.exe | 链接: https://pan.baidu.com/s/1Z07RibqhNiD7KNtRdz6gkw 提取码: ra5u |
| CocosCreator_v1.2.2-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.2.2/CocosCreator_v1.2.2_2016092001.dmg | 链接: https://pan.baidu.com/s/1sE_cAoEv3F6x9ZktKl5lZw 提取码: i5vg |
| CocosCreator_v1.2.1-win | http://cocostudio.download.appget.cn/CocosCreator/v1.2.1/CocosCreator_v1.2.1_2016081901_setup.exe | 链接: https://pan.baidu.com/s/1UqKSyD8hZlSjB2w88x4TaQ 提取码: hjh6 |
| CocosCreator_v1.2.1-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.2.1/CocosCreator_v1.2.1_2016081901.dmg | 链接: https://pan.baidu.com/s/10yV6zuoKpPb1iNPS4JZlhw 提取码: wbev |
| CocosCreator_v1.2.0-win | http://cocostudio.download.appget.cn/CocosCreator/v1.2.0/CocosCreator_v1.2.0_2016080301_setup.exe | 链接: https://pan.baidu.com/s/1eB4Ri6I6EwTQCdzJQ3S_XQ 提取码: q1ce |
| CocosCreator_v1.2.0-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.2.0/CocosCreator_v1.2.0_2016080301.dmg | 链接: https://pan.baidu.com/s/1084MbNt39jY32Bch7ti4wg 提取码: s63c |
| CocosCreator_v1.1.2-win | http://cocostudio.download.appget.cn/CocosCreator/v1.1.2/CocosCreator_v1.1.2_2016070802_setup.exe | 链接: https://pan.baidu.com/s/1_HCe4Uty4QVg2yxAseUETA 提取码: j7dg |
| CocosCreator_v1.1.2-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.1.2/CocosCreator_v1.1.2_2016070802.dmg | 链接: https://pan.baidu.com/s/1oEkAb47qv7XON_bqXx93Fg 提取码: shdi |
| CocosCreator_v1.1.1-win | http://cocostudio.download.appget.cn/CocosCreator/v1.1.1/CocosCreator_v1.1.1_2016061202_Setup.exe | 链接: https://pan.baidu.com/s/11DnjKmw3AmKviuSEQ5UiPA 提取码: nk5w |
| CocosCreator_v1.1.1-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.1.1/CocosCreator_v1.1.1_2016061202.dmg | 链接: https://pan.baidu.com/s/1WSj1Fzaxm6Gg1qhqR-P6IA 提取码: p6ii |
| CocosCreator_v1.1.0-win | http://cocostudio.download.appget.cn/CocosCreator/v1.1.0/CocosCreator_v1.1.0_20160531_Setup.exe | 链接: https://pan.baidu.com/s/1XA2s8S5ZRdh8zkj0cO_TNw 提取码: s4i6 |
| CocosCreator_v1.1.0-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.1.0/CocosCreator_v1.1.0_20160531.dmg | 链接: https://pan.baidu.com/s/1EC2kOQ1zF_rnECKT7B71Ng 提取码: c5fi |
| CocosCreator_v1.0.3-win | http://cocostudio.download.appget.cn/CocosCreator/v1.0.3/CocosCreator_v1.0.3_2016050901_Setup.exe | 链接: https://pan.baidu.com/s/11ha5LufWwAaZjfH9zlUeeQ 提取码: d421 |
| CocosCreator_v1.0.3-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.0.3/CocosCreator_v1.0.3_2016050901.dmg | 链接: https://pan.baidu.com/s/16voClyFeBb5e4aiFJHlnFg 提取码: 6dr3 |
| CocosCreator_v1.0.2-win | http://cocostudio.download.appget.cn/CocosCreator/v1.0.2/CocosCreator_v1.0.2_20160426_Setup.exe | 链接: https://pan.baidu.com/s/1PMT8nIkK7-jdTP7En-6v2w 提取码: u3tc |
| CocosCreator_v1.0.2-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.0.2/CocosCreator_v1.0.2_2016042602.dmg | 链接: https://pan.baidu.com/s/1bM1Oh3dq2gFrxBhJEdw1eQ 提取码: 1091 |
| CocosCreator_v1.0.1-win | http://cocostudio.download.appget.cn/CocosCreator/v1.0.1/CocosCreator_v1.0.1_2016041102_Setup.exe | 链接: https://pan.baidu.com/s/1kbKp6todCnaXyPGDqkAOzQ 提取码: v0ol |
| CocosCreator_v1.0.1-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.0.1/CocosCreator_v1.0.1_2016041102.dmg | 链接: https://pan.baidu.com/s/1NCEAyiz-_ASoCMLtM10kAg 提取码: 0jso |
| CocosCreator_v1.0.0-win | http://cocostudio.download.appget.cn/CocosCreator/v1.0.0/CocosCreator_v1.0.0_2016032904_Setup.exe | 链接: https://pan.baidu.com/s/1AhQ46xqfqcr_I8Rxiy_d0Q 提取码: 9plr |
| CocosCreator_v1.0.0-mac | http://cocostudio.download.appget.cn/CocosCreator/v1.0.0/CocosCreator_v1.0.0_2016032904.dmg | 链接: https://pan.baidu.com/s/1EKA8njz4arbglvwkMJdwLA 提取码: b89a |
| CocosCreator_v1.0.0-win_en | http://cocostudio.download.appget.cn/CocosCreator/v1.0.0/CocosCreator_v1.0.0_2016032904_Setup_en.exe | 链接: https://pan.baidu.com/s/1dS820G6KV69-31ZhqLuXEA 提取码: f8qk |
| CocosCreator_v1.0.0-mac_en | http://cocostudio.download.appget.cn/CocosCreator/v1.0.0/CocosCreator_v1.0.0_2016032904_en.dmg | 链接: https://pan.baidu.com/s/1vGOS3_yyy3dLNNwzBFImLQ 提取码: 48jf |