## CocosCreatorPlugin

### 配置说明
- excludedModules - engine 中需要排除的模块
- title - 项目名
- platform - 构建的平台 
构建的平台有 [web-mobile、web-desktop、android、win32、ios、mac、wechatgame、wechatgame-subcontext、baidugame、baidugame-subcontext、xiaomi、alipay、qgame、quickgame、huawei、cocosplay、fb-instant-games、android-instant]
- buildPath - 构建目录
- startScene - 主场景的 uuid 值（参与构建的场景将使用上一次的编辑器中的构建设置）
- debug - 是否为 debug 模式 [true、false]
- previewWidth - web desktop 窗口宽度
- previewHeight - web desktop 窗口高度
- sourceMaps - 是否需要加入 source maps
- webOrientation - web mobile 平台（不含微信小游戏）下的旋转选项 [landscape、portrait、auto]
- inlineSpriteFrames - 是否内联所有 SpriteFrame
- optimizeHotUpdate - 是否将图集中的全部 SpriteFrame 合并到同一个包中
- packageName - 包名
- useDebugKeystore - 是否使用 debug keystore
- keystorePath - keystore 路径
- keystorePassword - keystore 密码
- keystoreAlias - keystore 别名
- keystoreAliasPassword - keystore 别名密码
- orientation - native 平台（不含微信小游戏）下的旋转选项 [portrait, upsideDown, landscapeLeft, landscapeRight] 因为这是一个 object，所以定义会特殊一些：
- orientation={'landscapeLeft': true} 或 orientation={'landscapeLeft': true, 'portrait': true}
- template - native 平台下的模板选项 [default、link]
- apiLevel - 设置编译 android 使用的 api 版本

- appABIs - 设置 android 需要支持的 cpu 类型，可以选择一个或多个选项 [armeabi-v7a、arm64-v8a、x86]
因为这是一个数组类型，数据类型需要像这样定义，注意选项需要用引号括起来：
appABIs=['armeabi-v7a','x86']
- embedWebDebugger - 是否在 Web 平台下插入 vConsole 调试插件
- md5Cache - 是否开启 md5 缓存
- encryptJs - 是否在发布 native 平台时加密 js 文件
- xxteaKey - 加密 js 文件时使用的密钥
- zipCompressJs - 加密 js 文件后是否进一步压缩 js 文件
- autoCompile - 是否在构建完成后自动进行编译项目，默认为 否。
- configPath - 参数文件路径。如果定义了这个字段，那么构建时将会按照 json 文件格式来加载这个数据，并作为构建参数