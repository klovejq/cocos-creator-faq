module.exports = {
    'selectFBX': function () {
        let assetUuid = Editor.Selection.curSelection('asset');
        if (!assetUuid) {
            Editor.log('load asset uuid error !');
            return ;
        }

        // 检测面板焦点在资源管理器还是层级管理器
        let activeInfo = Editor.Selection.curGlobalActivate()
        // if (activeInfo && activeInfo.type == "node"){
        //     // 在层级管理器
        // }
        // if ((activeInfo && activeInfo.type == "asset")){
        //     // 在资源管理器
        // }

        if (!(activeInfo && activeInfo.type == "asset")){
            // 不在资源管理器
            Editor.Ipc.sendToPanel("fbx-preview", "clear-fbx-preview");
            return ;
        }

        Editor.assetdb.queryUrlByUuid(assetUuid, (err, assetPath) => {
            if (!err) {
                if (assetPath) {
                    // 判断 .fbx 文件后缀名，如果不是 .fbx 则不更新
                    let pathNames = assetPath.split('/');
                    let fileName = pathNames[pathNames.length - 1];
                    let suffixPath = fileName.split('.');
                    let suffixname = suffixPath[suffixPath.length - 1];
                    
                    if (suffixname == 'fbx' || suffixname == 'FBX') {
                        suffixPath.pop();
                        let prefabPath = assetPath + '/' + suffixPath + '.prefab';
                        let prefabUuid = Editor.remote.assetdb.urlToUuid(prefabPath);

                        Editor.log('Fbx uuid:' + assetUuid);
                        Editor.log('Fbx path:' + assetPath);
                        Editor.log('Fbx prefab path:' + prefabPath);
                        Editor.log('Fbx prefab uuid:' + prefabUuid);
                        
                        if (prefabUuid) {

                            // 找 .fbx 的资源，可能是 jpg 或者 png
                            let resPath = assetPath.substr(0, assetPath.length - 4);
                            Editor.log("Fbx update!");

                            let resSuffixPaths = [".jpg", ".png", ".jpeg", ".JPG", ".PNG", ".JPEG"];
                            for (let i = 0; i < resSuffixPaths.length; i++) {
                                let resp = resPath + resSuffixPaths[i];
                                let resUuid = Editor.remote.assetdb.urlToUuid(resp);
                                if (resUuid) {
                                    Editor.log('Fbx assets path:' + resp);
                                    Editor.log('Fbx assets uuid:' + resUuid);
                                    Editor.Ipc.sendToPanel("fbx-preview", "change-fbx-preview", prefabUuid, resUuid);
                                    return ;
                                }
                            }

                            Editor.Ipc.sendToPanel("fbx-preview", "change-fbx-preview", prefabUuid);
                        }
                    }
                }
            } 
        })
    }
};