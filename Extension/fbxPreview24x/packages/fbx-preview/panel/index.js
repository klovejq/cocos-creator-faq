let packageName = "fbx-preview";
let fs = require('fire-fs');

// panel/index.js, this filename needs to match the one registered in package.json
Editor.Panel.extend({
  // css style for panel
  style: fs.readFileSync(Editor.url('packages://' + packageName + '/panel/index.css', 'utf8')) + "",
  // html template for panel
  template: fs.readFileSync(Editor.url('packages://' + packageName + '/panel/index.html', 'utf8')) + "",

  // element and variable binding
  $: {
    view: '#view',
  },

  // method executed when template and styles are successfully loaded and initialized
  ready () {
    setTimeout(()=>{
      let port = Editor.remote.PreviewServer._previewPort;
      this.$view.src = `http://localhost:${port}/`;
    }, 3000);
  },

  messages: {
    // 清理 FBX 预览界面
    'clear-fbx-preview' () {
      this.$view.contentWindow.postMessage({ msg:
        `
        if (cc.director.getScene().name !== "preview-scene") {
          cc.director.loadScene("preview-scene");
        }
        let modelRoot = cc.find("Canvas/modelRoot");
        let len = modelRoot.childrenCount;
        if (len > 0) {
          modelRoot.destroyAllChildren();
        }

        // UI 处理
        let tipNode = cc.find("Canvas/tip");
        if (tipNode && !tipNode.active) {
          tipNode.active = true;
        }
        let scaleSliderNode = cc.find("Canvas/scaleSlider");
        if (scaleSliderNode && scaleSliderNode.active) {
          scaleSliderNode.active = false;
        }
        let lrSliderNode = cc.find("Canvas/lrSlider");
        if (lrSliderNode && lrSliderNode.active) {
          lrSliderNode.active = false;
        }
        let ubSliderNode = cc.find("Canvas/ubSlider");
        if (ubSliderNode && ubSliderNode.active) {
          ubSliderNode.active = false;
        }
        `
      },'\*');
    },

    // 替换 FBX 预览界面
    'change-fbx-preview'(meta, uuid, resuuid) {

      // iFrame 发送交互数据  注入代码
      this.$view.contentWindow.postMessage({ msg:
        `
        if (cc.director.getScene().name !== "preview-scene") {
          cc.director.loadScene("preview-scene");
        }
        cc.assetManager.loadAny("${uuid}", (err,res) => {
            if (err) {
              console.log("load ${uuid} err, msg:", err);
              return;
            }

            let modelRoot = cc.find("Canvas/modelRoot");
            let len = modelRoot.childrenCount;
            if (len > 0) {
              modelRoot.destroyAllChildren();
            }

            let factor = 100;
            let lrAngle = 0;
            let ubAngle = 0;
            // UI 处理
            let tipNode = cc.find("Canvas/tip");
            if (tipNode && tipNode.active) {
              tipNode.active = false;
            }

            // 放大系数
            let scaleSliderNode = cc.find("Canvas/scaleSlider");
            if (scaleSliderNode) {
              scaleSliderNode.active = true;

              let scaleSlider = scaleSliderNode.getComponent(cc.Slider);
              factor = (scaleSlider.progress / 0.001) + 1;
            }
            let a = cc.instantiate(res);  
            modelRoot.addChild(a); 
            a.scale = factor;
            a.name = "model";

            // 左右偏移
            let lrSliderNode = cc.find("Canvas/lrSlider");
            if (lrSliderNode) {
              lrSliderNode.active = true;

              let lrSlider = lrSliderNode.getComponent(cc.Slider);
              lrAngle = (lrSlider.progress - 0.5) * 360;
            }
            a.rotationY = lrAngle;

            // 上下偏移
            let ubSliderNode = cc.find("Canvas/ubSlider");
            if (ubSliderNode) {
              ubSliderNode.active = true;

              let ubSlider = ubSliderNode.getComponent(cc.Slider);
              ubAngle = (ubSlider.progress - 0.5) * 360;
            }
            a.rotationX = ubAngle;

            let resuuid = "${resuuid}";
            if (!resuuid) return;

            cc.assetManager.loadAny("${resuuid}", (err, tex) => {
              if (err) {
                console.log("load ${resuuid} err, msg:", err);
                return;
              }

              let renders = a.getComponentsInChildren(cc.MeshRenderer);

              for (let j = 0; j < renders.length; j++) {
                let mat = renders[j].getMaterial(0);
                let newMat = cc.MaterialVariant.create(mat);
                newMat.define("USE_DIFFUSE_TEXTURE", true);
                newMat.setProperty('diffuseTexture', tex, 0);
                renders[j].setMaterial(0, newMat);
              }
            });
        })`
      },'\*');

    }
  }

});