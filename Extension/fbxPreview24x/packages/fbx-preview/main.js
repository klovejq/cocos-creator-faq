'use strict';

module.exports = {
  load () {
    // execute when package loaded
  },

  unload () {
    // execute when package unloaded
  },

  // register your ipc messages here
  messages: {
    'open' () {
      // open entry panel registered in package.json
      Editor.log('Open the FBX Preview !');
      Editor.Panel.open('fbx-preview');
      Editor.Scene.callSceneScript('fbx-preview', 'selectFBX');
    },

    'scene:saved' () {
      Editor.Scene.callSceneScript('fbx-preview', 'selectFBX');
    },

    '_selection:selected' () {
      Editor.Scene.callSceneScript('fbx-preview', 'selectFBX');
    },
  },
};