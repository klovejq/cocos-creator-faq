const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    private _scaleBase : number = 1;
    private _isDrag : boolean = false;

    start () {
        let tipNode = cc.find("Canvas/tip");
        if (tipNode && !tipNode.active) {
          tipNode.active = true;
        }
        let scaleSliderNode = cc.find("Canvas/scaleSlider");
        if (scaleSliderNode && scaleSliderNode.active) {
          scaleSliderNode.active = false;
        }
        let lrSliderNode = cc.find("Canvas/lrSlider");
        if (lrSliderNode && lrSliderNode.active) {
          lrSliderNode.active = false;
        }
        let ubSliderNode = cc.find("Canvas/ubSlider");
        if (ubSliderNode && ubSliderNode.active) {
          ubSliderNode.active = false;
        }

        let view = cc.find("Canvas/view");
        if (view) {
          view.on(cc.Node.EventType.MOUSE_DOWN, this.onMouseDown, this);
          view.on(cc.Node.EventType.MOUSE_MOVE, this.onMouseMove, this);
          view.on(cc.Node.EventType.MOUSE_WHEEL, this.onMouseWheel, this);
        }
    }

    onMouseDown (e: cc.Event.EventMouse) {
      let btn = e.getButton();
      if (btn == null || btn == undefined) return;

      switch (btn) {
        case cc.Event.EventMouse.BUTTON_RIGHT:
          this._isDrag = true;
        break;
        case cc.Event.EventMouse.BUTTON_LEFT:
          this._isDrag = false;

          let model = cc.find('Canvas/modelRoot/model');
          if (model) {
            let pos = e.getLocation();
            let viewSize = cc.view.getVisibleSize();
            pos.x -= viewSize.width / 2;
            pos.y -= viewSize.height / 2;

            model.setPosition(pos);
          }
        break;
      }
    }

    onMouseMove (e: cc.Event.EventMouse) {
      if (!this._isDrag) return;
      
      let model = cc.find('Canvas/modelRoot/model');
      if (model) {
        let pos = e.getLocation();
        let viewSize = cc.view.getVisibleSize();
        pos.x -= viewSize.width / 2;
        pos.y -= viewSize.height / 2;

        model.setPosition(pos);
      }
    }

    onMouseWheel (e: cc.Event.EventMouse) {
      let offsetY = e.getScrollY();

      let model = cc.find('Canvas/modelRoot/model');
      if (model) {
        let offset = offsetY / 5;

        let scale = model.scale + offset;
        if (scale > 1 && scale < 1002) {
          model.scale = scale;
        }
      }
    }

    onSlider (s: cc.Slider) {
        let scale = s.progress;

        let diff = scale - 0;
        let factor = (diff / 0.001);

        let model = cc.find('Canvas/modelRoot/model');
        if (model) {
            model.scale = this._scaleBase + factor;
        }

        let scNode = cc.find('Canvas/scaleSlider/label');
        if (scNode) {
          let label = scNode.getComponent(cc.Label);
          label.string = "scale * " + Math.round(this._scaleBase + factor);
        }
    }


    onLRSlider (s: cc.Slider) {
      let scale = s.progress;
      let diff = scale - 0.5;
      let factor = 360;

      let model = cc.find('Canvas/modelRoot/model');
      if (model) {
          model.rotationY = factor * diff;
      }

      let lrNode = cc.find('Canvas/lrSlider/label');
      if (lrNode) {
        let label = lrNode.getComponent(cc.Label);
        label.string = Math.round(factor * diff) + "°";
      }
    }

    onUBSlider (s: cc.Slider) {
      let scale = s.progress;
      let diff = scale - 0.5;
      let factor = 360;

      let model = cc.find('Canvas/modelRoot/model');
      if (model) {
          model.rotationX = factor * diff;
      }

      let lrNode = cc.find('Canvas/ubSlider/label');
      if (lrNode) {
        let label = lrNode.getComponent(cc.Label);
        label.string = Math.round(factor * diff) + "°";
      }
    }

    onEditBoxDid (e: cc.EditBox) {
      let str = e.string;
      if (!str || str.length <= 0) return;

      let v = parseInt(str);
      if (v < 0) return;

      let model = cc.find('Canvas/modelRoot/model');
      if (model) {
          model.scale = v;
      }
    }
}
