const fs = require('fs');
const path = require('path');

// 面板的内容
exports.template = fs.readFileSync(path.join(__dirname, 'pannel.html'), 'utf-8');

// 快捷选择器
exports.$ = {
    image: '.image',
    output: '.output',
    btn: '.blue'
};

// 面板启动后触发的钩子函数
exports.ready = async function () {
    let config = await Editor.Message.request('icon-generate', 'read');
    if (config) {
        this.$.image.value = config.image || '';
        this.$.output.value = config.output || '';
    }
    this.$.btn.addEventListener('confirm', () => {
        config = {
            image: this.$.image.value,
            output: this.$.output.value,
        }
        Editor.Message.send('icon-generate', 'save', config);
    });

};

// 面板准备关闭的时候会触发的函数，return false 的话，会终止关闭面板
exports.beforeClose = function () {
};

// 面板关闭后的钩子函数
exports.close = function () { };