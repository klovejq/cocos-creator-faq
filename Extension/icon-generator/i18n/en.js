module.exports = {
    title: 'icon-generate',
    openSetPannel: 'Setting Pannel Opened',
    continue: 'continue',
    selectTip: 'png or jpg',
    selectTooltip: 'Suggest upload 1024px',
    cancel: 'cancel',
    start: 'Starting',
    end: 'Success',
    setPannel: 'Setting Pannel',
    selectImages: 'Image File',
    selectDirectory: 'Output',
    generate: 'generate',
    error: 'Generate Error',
    noImage: 'Please select a image',
    noOutput: 'Please select output folder',
    directorTitle: 'Folder existed',
    directorTip: 'The Android and iOS folders will be overwritten'
}