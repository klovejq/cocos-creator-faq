
## 扩展

| NO. | Author | Desc | Proj | Mark |
| :---: | :---: | :---: | :---: | :---: |
| 1 | xu_yanfeng | 快速打开官方文档 | [cocos-helper](https://gitee.com/yeshao2069/cocos-creator-plugin/tree/master/creator-helper) |  |
| 2 | dream93 | icon图标生成器 | [con-generator](https://gitee.com/yeshao2069/cocos-creator-plugin/tree/master/icon-generator) | [原文链接](https://forum.cocos.org/t/topic/109089) |
| 3 | VisualSJ | 股票小助手 | [stock-helper](https://gitee.com/yeshao2069/cocos-creator-plugin/tree/master/stock-helper) | Creator 3.x |
| 4 | xu_yanfeng | Excel转JS和JSON | [excel-killer](https://gitee.com/yeshao2069/cocos-creator-plugin/tree/master/excel-killer) | Creator 2.4.x、[原文链接](https://forum.cocos.org/t/topic/57352) |
| 5 | Cocos Creator | 检查模型2UV | [check2uv](https://gitee.com/yeshao2069/cocos-creator-plugin/tree/master/check2uv) | Creator 3.4.x |
| 6 | muxiandong | 支持模型预览 | [fbxPreview24x](https://gitee.com/yeshao2069/cocos-creator-plugin/tree/master/fbxPreview24x) | Creator 2.4.x |

### 使用方式
- 全局
MacOS 存放在系统下的 .CocosCreator 路径下的 extensions 文件夹，如 /Users/apple/.CocosCreator/extensions 文件夹下    
Window 存放在系统C盘 /User/用户/.CocosCreator路径下的extensions 文件夹，C:\Users/24754/.CocosCreator\extensions

- 项目
项目工程的 extensions 文件夹下