## Cocos Creator FAQ

### 支持在编辑器上运行 Spine 2.x

### 操作
    
    - 1 引入SpineTool.js 插件，勾选为插件-允许在编辑器加载
    - 2 需要为spine动画添加挂点

### 预览
![image](../../../pic/gif/202203/2022030202.gif)