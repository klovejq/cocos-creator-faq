## 插件脚本

* 简介
    
    定制化插件，将插件脚本放置到项目下，导入为插件，设置为顶层。

| 编号 | 类型 | 备注 |
| :---: | :---: | :---: |
| 1 | [3.x / 修改调试信息颜色](./debug-message) | 将[debugMessageColorModify.js](./debug-message/debugMessageColorModify.js)插件脚本放置项目下，并允许web加载 |
| 2 | [3.x / 允许Graphics修改全局透明度](./graphics-global-opacity/) | 将[SupportGraphicsOpacity.js](./graphics-global-opacity/SupportGraphicsOpacity.js)插件脚本放置项目下，并允许web、native加载 |
| 3 | [2.x / 允许编辑器预览Spine](./spine-run-in-editor/demo/SupportSpineRunInEditor2x/) | 将[SupportSpineRunInEditor2X.js](./spine-run-in-editor/SupportSpineRunInEditor2X.js)插件脚本放置项目下，并允许编辑器加载 |
| 4 | [3.x / 允许编辑器预览Spine](./spine-run-in-editor/demo/SupportSpineRunInEditor3x/) | 将[SupportSpineRunInEditor3X.js](./spine-run-in-editor/SupportSpineRunInEditor3X.js)插件脚本放置项目下，并允许编辑器加载 |