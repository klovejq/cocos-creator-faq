## Cocos Creator FAQ
### Services - 服务

---
### Q1 
#### Question: frontjs 导入项目到位置是否可以自定义
R: frontjs 导入到项目位置是否可以自定义路径？每次修改之后拖拽重新生成的路径?    

A: frontjs 导入到项目位置可以自定义路径, 生成插件脚本后就可以自定义路径了，不需要特殊处理。重新生成的路径无法修改, 那个是插件已经处理好的。

---
### Q2 
#### Question: Cocos Creator 接入 FrontJS SDK 怎么查询报错
A: 导出 sourcemap 文件，配合压缩后的 js 文件来定位源码位置，可以参考 https://blog.csdn.net/jm1999/article/details/84921884 , 在 line 和 column 填入对应的行列即可对应到报错源代码
```
var fs = require('fs'),
  path = require('path'),
  sourceMap = require('source-map')
 
// 要解析的map文件路径./test/vendor.8b1e40e47e1cc4a3533b.js.map
var GENERATED_FILE = path.join(
  '.',
  'test',
  'vendor.8b1e40e47e1cc4a3533b.js.map'
)
// 读取map文件，实际就是一个json文件
    var rawSourceMap = fs.readFileSync(GENERATED_FILE).toString();
    // 通过sourceMap库转换为sourceMapConsumer对象
    var consumer = await new sourceMap.SourceMapConsumer(rawSourceMap);
    // 传入要查找的行列数，查找到压缩前的源文件及行列数
    var sm = consumer.originalPositionFor({
        line: 2,  // 压缩后的行数
        column: 100086  // 压缩后的列数
      });
    // 压缩前的所有源文件列表
    var sources = consumer.sources;
    // 根据查到的source，到源文件列表中查找索引位置
    var smIndex = sources.indexOf(sm.source);
    // 到源码列表中查到源代码
    var smContent = consumer.sourcesContent[smIndex];
    // 将源代码串按"行结束标记"拆分为数组形式
    const rawLines = smContent.split(/\r?\n/g);
    // 输出源码行，因为数组索引从0开始，故行数需要-1
    console.log(rawLines[sm.line - 1]);
```