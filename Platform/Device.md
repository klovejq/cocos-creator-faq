## 设备

### 低端机列表
| 序号 | 系统 | 设备品牌 | 设备型号 | 设备分辨率 |
| :--- | :---: | :---: | :---: | :---: |
| 1 | 安卓 | HUAWEI | 荣耀20 | 1080x2340 |
| 2 | 安卓 | HUAWEI | 华为畅享10 Plus | 1080x2340 |
| 3 | 安卓 | HUAWEI | 华为P9 | 1080x1920 |
| 4 | 安卓 | HUAWEI | 荣耀8青春版 | 1080x1920 |
| 5 | 安卓 | HUAWEI | 华为Mate 10 Pro（全网通） | 1080x2160 |
| 6 | 安卓 | HUAWEI | 荣耀9X | 1080x2340 |
| 7 | 安卓 | HUAWEI | 荣耀10 | 1080x2280 |
| 8 | 安卓 | HUAWEI | 荣耀V10 | 1080x2160 |
| 9 | 安卓 | HUAWEI | 荣耀畅玩7X | 1080x2160 |
| 10 | 安卓 | HUAWEI | 荣耀畅玩9A | 720x1600 |
| 11 | 安卓 | HUAWEI | 华为P20 Pro | 1080x2240 |
| 12 | 安卓 | HUAWEI | 荣耀8青春版 | 1080x1920 |
| 13 | 安卓 | HUAWEI | 荣耀9 | 1080x1920 |
| 14 | 安卓 | HUAWEI | 华为nova 3 | 1080x2340 |
| 15 | 安卓 | HUAWEI | 华为P20 | 1080x2244 |
| 16 | 安卓 | HUAWEI | 华为P10 Plus | 1440x2560 |
| 17 | 安卓 | HUAWEI | 荣耀8 | 1080x1920 |
| 18 | 安卓 | HUAWEI | 华为畅享7 Plus | 720x1280 |
| 19 | 安卓 | XiaoMI | 小米6X | 1080x2160 |
| 20 | 安卓 | XiaoMI | 小米Max 2（全网通） | 1080x1920 |
| 21 | 安卓 | XiaoMI | Redmi Note 8 | 1080x2340 |
| 22 | 安卓 | XiaoMI | 小米Note 3（6GB RAM/全网通） | 1080x1920 |
| 23 | 安卓 | XiaoMI | 小米5X（全网通） | 1080x1920 |
| 24 | 安卓 | XiaoMI | 小米红米Note 5 | 1080x2160 |
| 25 | 安卓 | XiaoMI | 小米红米Note 3（高配版/全网通） | 1080x1920 |
| 26 | 安卓 | VIVO | VIVO Y93 | 720x1520 |
| 27 | 安卓 | VIVO | VIVO X9 Plus | 1080x1920 |
| 28 | 安卓 | VIVO | VIVO Y5s | 1080x2340 |
| 29 | 安卓 | VIVO | VIVO X20 | 1080x2160 |
| 30 | 安卓 | VIVO | VIVO X9 | 1080x1920 |
| 31 | 安卓 | VIVO | VIVO Y85 | 1080x2280 |
| 32 | 安卓 | VIVO | VIVO Y67 | 720x1280 |
| 33 | 安卓 | VIVO | VIVO X23炫彩版 | 1080x2340 |
| 34 | 安卓 | VIVO | VIVO X21 | 1080x2280 |
| 35 | 安卓 | VIVO | VIVO Y50 | 1080x2340 |
| 36 | 安卓 | VIVO | VIVO X7（全网通） | 1080x1920 |
| 37 | 安卓 | VIVO | VIVO X7 Plus（全网通） | 1080x1920 |
| 38 | 安卓 | VIVO | VIVO Y9s | 1080x2340 |
| 39 | 安卓 | VIVO | VIVO X21屏幕指纹版 | 1080x2280 |
| 40 | 安卓 | VIVO | VIVO Y51 | 540x960 |
| 41 | 安卓 | VIVO | VIVO Y55 | 720x1280 |
| 42 | 安卓 | VIVO | VIVO Y66 | 720x1280 |
| 43 | 安卓 | OPPO | OPPO R15 | 1080x2280 |
| 44 | 安卓 | OPPO | OPPO A9 | 1080x2340 |
| 45 | 安卓 | OPPO | OPPO A5 | 720x1520 |
| 46 | 安卓 | OPPO | OPPO R11 | 1080x1920 |
| 47 | 安卓 | OPPO | OPPO A59s | 720x1280 |
| 48 | 安卓 | OPPO | OPPO R11s | 1080x2160 |
| 49 | 安卓 | OPPO | Oppo A8 | 720x1600 |
| 50 | IOS | iPhone | iPhone 8 Plus | 1920x1080 |
| 51 | IOS | iPhone | iPhone 8 | 1334x750 |
| 52 | IOS | iPhone | iPhone X | 2436x1125 |
| 53 | IOS | iPhone | iPhone 7 | 1334x750 |
| 54 | IOS | iPhone | iPhone 6s | 1334x750 |
| 55 | IOS | iPhone | iPhone 6s Plus | 1920x1080 |
| 56 | IOS | iPhone | iPhone 7 Plus | 1920x1080 |
| 57 | IOS | iPhone | iPhone 6 | 1334x750 |