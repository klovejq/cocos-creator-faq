## Cocos Creator FAQ
### Web

---
#### Q1
##### Question: 如何去除加载页    
##### Version: Cocos Creator 2.x & 3.x    
Answer:    

2.x 版本   
```ts
1.1 构建 web 完成之后，修改 index.html 中的 L19 splash.style.display
> splash.style.display = 'none';

1.2 修改 main.xxx.js 中的 L19 splash.style.display
> splash.style.display = 'none';

1.3 修改 style-mobile.xxx.css 和 style-desktop.xxx.css，新增
>#GameCanvas {
    width: 0%;
    height: 0%;
}
```
3.x版本
```ts
构建 Web 版本的时候, 替换插屏 功能勾选, 设置显示时间为 0
```

---
#### Q2
##### Question: 修改web的vconsole    
Question Addtion:    

打包Cocos Creator 2.4.6 版本，需要修改 vconsole 按钮的位置  
##### Version: Cocos Creator 2.4.6  
Answer:   

修改 vconsole.min.js 文件，可以搜索 __vconsole .vc-switch
```
#__vconsole .vc-switch {margin-bottom: 400px; display:block ... }
```
或者在 main.js 脚本动态修改该 CSS 的 style
```
var vc_switch = document.getElementsByClassName('vc-switch');
setInterval(() => { vc_switch[0].style.width = '100%';}, 100)
```

---
#### Q3
##### Question: 写入数据到文件   
Question Addtion:    

为了开发测试写一些测试数据，需要吧数据写入到某个 txt 文本中。  
##### Version: Cocos Creator All        
Answer:   

原生平台可以使用`jsb.FileUtils.writeStringToFile`写入数据。Web 平台没有办法访问文件系统，这个是浏览器限制没办法突破。可以考虑使用本地缓存`sys.localStorage`

---
#### Q4
##### Question: Cocos Creator为什么发布web后，空工程也会CPU占用比较高?    
##### Version: Cocos Creator All  
Answer:    

浏览器屏幕刷新本身就会有开销的，虽然什么都不做，但实际上也是 60 fps 满帧在跑。建议试试发布成安卓原生，应该开销会小一些。 @jare https://forum.cocos.org/t/topic/126233/2

---
#### Q5
##### Question: Cocos Creator 3.3.2 升级到 3.4.0社区版报错提示(Error: UI element #step-length dosen't exist.)
##### Version: Cocos Creator 3.4.0
Answer:    

Web上预览时报错这个提示，是因为项目运行时使用了运行时查看节点树的插件导致的。这个插件可能暂时不兼容3.4.0版本，所以异常报错。处理方式是删除这个插件即可。