## Cocos Creator FAQ
#### Write by yeshao2069.
#### Cocos Creator FAQ is designed to help users deal with some of their common daily problems.
&nbsp;


发布Cocos Creator来发布全屏的windows游戏客户端，默认是窗口模式的。要实现全屏需要改动Cocos Creator提供的desktop端构建源码，要修改的文件为：

```
# 通过glfw创建全屏窗口
C:\CocosCreator\resources\cocos2d-x\cocos\platform\desktop\CCGLView-desktop.cpp
```
以及
```
# 通过传递尺寸参数修改视觉窗口，此文件在构建发布windows版本的时候生成
# 即需要Cocos Creator构建工程后才会生成
E:\Cocos\HelloWindows\build\jsb-link\frameworks\runtime-src\proj.win32\main.cpp
```


在CCGLView-desktop.cpp中，找到GLView::GLView构造函数，对原来的窗口生成代码进行修改

```
_mainWindow = glfwCreateWindow(screen_width, screen_height, name.c_str(), _monitor, nullptr);
```

修改为

```
const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
int screen_width = mode->width;
int screen_height = mode->height;
// _mainWindow = glfwCreateWindow(1920, 1080, name.c_str(), glfwGetPrimaryMonitor(), nullptr);
_mainWindow = glfwCreateWindow(screen_width, screen_height, name.c_str(), glfwGetPrimaryMonitor(), nullptr);
// _mainWindow = glfwCreateWindow(screen_width, screen_height, name.c_str(), _monitor, nullptr);
```

即，第四个参数传入glfwGetPrimaryMonitor默认显示器就可以创建全屏窗口，可以看到程序会自动识别屏幕分辨率，根据屏幕分辨率创建全屏窗口，但是在main.cpp文件中是固定的960*640，所以需要根据需要对其进行修改，否则视觉窗口与游戏内容不匹配。

```
// main.cpp文件中修改构造函数传入参数，分辨率根据实际情况进行修改
AppDelegate app(1920, 1080);
```

修改完成后再次点击构建工程和编译，Cocos Creator会在E:\Cocos\Animal\build\jsb-link\publish\win32目录下发布程序，拷贝全部内容可在其他win10电脑上运行。

- 刚构建的win32工程可能存在sdk版本问题编译不了，需要使用visual studio 2017来打开E:\Cocos\Animal\build\jsb-link\frameworks\runtime-src\proj.win32目录下的win32工程，当提示更新时，选择是即可，然后再次用Cocos Creator对工程进行编译。