## Cocos Creator FAQ
### Mini Game / 小游戏 (微信小游戏、字节小游戏、vivo、oppo、小米、华为等)

### FAQ
| 编号 | 子项 | 备注 |
| :---: | :---: | :---: |
| 1 | [3.4.0 构建小游戏运行报错 (TypeError: Cannot read property 'prototype' of undefined](./faq.md#q1) | FAQ问答 |
| 2 | [阴影显示异常](./faq.md#q2) | FAQ问答 |
| 3 | [动态加载的音效播放没有声音](./faq.md#q3) | FAQ问答 |
| 4 | [使用 es6 语法发布微信小游戏报错 'Unhandled promise rejection ReferenceError: regeneratorRuntime is not defined'](./faq.md#q4) | FAQ问答 |
| 5 | [微信小游戏报错 'TypeError: A.__uid is not a function at Object.texSubImage2D (magicbrush.js:1:17083)'](./faq.md#q5) | FAQ问答 |
| 6 | [视频广告结束奖励无法正常发放](./faq.md#q6) | FAQ问答 |

### Customize
| 编号 | 子项 | 备注 |
| :---: | :---: | :---: |
| 1 | [支持Vivo小游戏错误堆栈输出](./SupportPrintErrorStack2x) | 支持2.4.x |