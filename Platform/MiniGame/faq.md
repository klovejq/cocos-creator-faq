## Cocos Creator FAQ

### Mini Game / 小游戏 (微信小游戏、字节小游戏、vivo、oppo、小米、华为等)

---
#### Q1
##### Question: CocosCreator 3.4.0 构建微信小游戏和字节小游戏运行报错(TypeError: Cannot read property 'prototype' of undefined)
##### Version: Unknown
Answer:    

项目中脚本存在循环引用，导致基类找不到产生的报错。

---
#### Q2
##### Question: 小游戏内阴影显示异常(阴影变成小黑圆圈)
##### Version: Unknown
Answer:    

3D 节点上可能存在 UITransform 和 Widget 组件(这适用于 UI 的组件)导致的异常，移除 UI 组件后重试。

---
#### Q3
##### Question: 动态加载的音效播放没有声音
Question Addtion:    

CocosCreator 2.0.10, 使用`cc.loader.loadRes`加载的 audioClip 在部分手机上没有声音，比如 RedMI K30s Ultra上, 查看加载之后的 Audio 属性，一些参数是 undefined。出问题的音频有提示过“音频文件已损坏”, 使用播放器播放是可以的, 怀疑是之前用 apple music 剪辑后压缩导致的。    
##### Version: CocosCreator 2.0.10
Answer:    

可以尝试修改音频的采样率为`44.1KHZ`。因为在当前的主流采集卡上，音频的采样频率一般共分为 22.05KHZ、 44.1KHZ、48KHZ 三个等级, 其中 22.05KHZ 只能达到 FM 广播的声音品质, 44.1KHZ 则是理论上的 CD 音质界限, 48KHZ 则更加精确一些, 高于 48KHZ 的采样频率人耳就无法分辨出来了。 另外可以先用原本没有处理过的原音频播放试试, 可以排除剪辑压缩导致的问题。

---
#### Q4
##### Question: 使用es6语法发布微信小游戏报错'Unhandled promise rejection ReferenceError: regeneratorRuntime is not defined'
Question Addtion:    

CocosCreator 3.1.2，使用到了同步执行的Promise，报错 'Unhandled promise rejection ReferenceError: regeneratorRuntime is not defined'    
##### Version: CocosCreator 3.1.2
Answer:    

首先确认打开自动转 ES5 微信开发者工具->本地设置->将JS编译成ES5 的选项需要勾选。其次需要确保脚本文件小于`500k`，因为超过500k 的脚本文件无法转换成 ES5。最后需要打开增加编译 微信开发者工具->本地设置->增加编译。

---
#### Q5
##### Question: 微信小游戏报错 'TypeError: A.__uid is not a function at Object.texSubImage2D (magicbrush.js:1:17083)'
Question Addtion:    

CocosCreator 3.4.0 打包微信小游戏报错，之前3.3.2版本以及之前的版本没有报错，并且游戏会闪退。    
##### Version: CocosCreator 3.4.0
Answer:    

检查一下是不是因为在微信小游戏里使用了子域导致的问题(隐藏子域的节点看是否运行正常)，其次检查子域获取的传值是否正确，是否在新版本里面使用的是之前生成的子域模板，如果是的话，需要重新生成子域模板。另外需要在构建微信小游戏的时候，勾选`生成开发数据与工程模板`选项。

---
#### Q6
##### Question: 视频广告结束后触发了断线重连，导致奖励无法正常发放？
Question Addtion:    

看完视频广告之后关闭广告页面以后向服务器发送发放视频奖励协议，但是触发了onClose导致无法收到服务器返回的消息, 触发onClose会执行断线重连逻辑。    
##### Version: CocosCreator All
Answer:    

这个问题就需要自行在断线重连之后查询服务器，是否存在奖励未领取的情况。