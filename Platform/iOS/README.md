## Cocos Creator FAQ
### iOS - 苹果

### FAQ
| 编号 | 子项 | 备注 |
| :---: | :--- | :---: |
| 1 | [JSB 绑定失效](./faq.md#q1) | FAQ问答 |
| 2 | [提示“不支持的URL”](./faq.md#q2) | FAQ问答 |
| 3 | [设置 pvr 并勾选 genMipmaps 运行报错](./faq.md#q3) | FAQ问答 |
| 4 | [Xcode 中导入 framework 库不成功](./faq.md#q4) | FAQ问答 |