## Cocos Creator FAQ

### iOS - 苹果

---
#### Q1
##### Question: JSB 自动绑定, C++ 代码失效?
Question Addtion:    

iOS平台, 写了两个C++代码的类, 也使用了 #include 了, 但是jsb调用的时候，提示类没有定义    
##### Version: Creator 2.0.10
Answer:     

查看编译有没有报错, 是否是代码写的问题。查看实际编译是否生效, 直接在xcode上调试代码即可。比如 register_all_spine 是一个注册回调函数，你加个断点看看，是否有进度断点。另外，如果是 #include 引用是 加在Classes/jsb_module_register.cpp 里面, 需要修改加到 manual/jsb_module_register.cpp 里面。因为 在Classes下的文件每次构建都会被覆盖。

---
#### Q2
##### Question: 提示"不支持的 URL"
Question Addtion:    

iOS发送字母和数字没有问题，发送中文就会有 "Response failed, error buffer: 不支持的URL" 提示    
##### Version: Creator 2.4.7
Answer:     

iOS平台需要自行做下 urlEncode 操作，不然URL的中文会解析异常。

---
#### Q3
##### Question: 设置 pvr 并勾选 genMipmaps 运行报错?
Question Addtion:    

2.4.x版本，设置 PVR 并勾选 Gen Mipmaps，构建 web 平台，在 iOS 上运行报错。    
##### Version: Creator 2.4.x
Answer:     

pvr 不支持 gl.generateMipmap, pvr 的 mipmap 是用工具离线生成的，保存在 pvr 文件里面的。Creator 编辑器目前无法为 pvr 生成 mipmap，需要自行二次处理一下图片资源。 @link https://programmerclick.com/article/6628594252/

---
#### Q4
##### Question: Xcode 中导入 framework 库不成功？
##### Version: Creator 3.5.0
Answer:    

这是引擎的 cmake 模板有问题导致的 BUG。用户设置的 CC_UI_RESOURCES 没有使用被合并导致的。可以使用 set(CC_UI_RESOURCES, ${CC_UI_RESOURCES} xxx) 处理。该问题会在 3.5.1 版本上解决。    
修复的 PR https://github.com/cocos/cocos-engine/pull/11011/files    
cmake 模板的位置    
- Windows: D:\CocosDashboard\resources\.editors\Creator\3.5.0\resources\resources\3d\engine-native\templates\cmake
- macOS: /Applications/CocosCreator/Creator/3.5.0/CocosCreator.app/Contents/Resources/resources/3d/engine/native/templates/cmake