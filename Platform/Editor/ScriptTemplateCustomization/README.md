## Cocos Creator FAQ

### 简介
脚本模板自定义

### 使用
- 找到引擎安装路径下，修改ts文件
    > Windows系统  E:\CocosDashboard\resources\.editors\Creator\3.0.0\resources\resources\3d\engine\editor\assets\default_file_content
    > 
    > macOS系统  /Applications/CocosCreator/Creator/3.0.0/CocosCreator.app/Contents/Resources/resources/3d/engine/editor/assets/default_file_content/ts

![image](../../../pic/image/202206/3033063021.png)
![image](../../../pic/image/202206/3033063022.png)
- 修改ts文件为需要的模板样式，一般推荐

import { _decorator, Component } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('<%Name%>')
export class <%Name%> extends Component {

    start () {

    }

    onLoad () {

    }

    update (deltaTime: number) {

    }
}
- 在编辑器中，新建一个ts脚本，就变成默认的ts脚本模板了

![image](../../pic/image/202206/3033063023.png)