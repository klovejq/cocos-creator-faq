## Cocos Creator FAQ
### Editor / 编辑器


### Usage
| 编号 | 子项 | 备注 |
| :---: | :---: | :---: |
| 1 | [编辑器面板-项目设置-全局配置](./Editor#1) | 基础使用 |
| 2 | [命令行打开工程](./Editor#2) | 基础使用 |
| 3 | [关闭自动刷新脚本](./Editor#3) | 基础使用 |

### FAQ
| 编号 | 子项 | 备注 |
| :---: | :---: | :---: |
| 1 | [属性面板定义二维数组](./Editor/faq.md#q1) | FAQ问答 |
| 2 | [3.4.0 构建报错 Cannot read property 'type' of undefined @getDependUuidsDeep](./Editor/faq.md#q2) | FAQ问答 |
| 3 | [2.x Setting 文件中的 packedAssets 的生成规则](./Editor/faq.md#q3) | FAQ问答 |
| 4 | [构建后资源压缩比例](./Editor/faq.md#q4) | FAQ问答 |
| 5 | [2.x 关闭资源刷新](./Editor/faq.md#q5) | FAQ问答 |
| 6 | [同一个项目构建不同的安卓包](./Editor/faq.md#q6) | FAQ问答 |
| 7 | [Dashboard 导入 2.0.10 项目失败](./Editor/faq.md#q7) | FAQ问答 |
| 8 | [压缩纹理配置里面的百分数的含义](./Editor/faq.md#q8) | FAQ问答 |
| 9 | [3.x 图片无法拖到层级管理器上](./Editor/faq.md#q9) | FAQ问答 |
| 10 | [定位模块中未勾选的模块](./Editor/faq.md#q10) | FAQ问答 |
| 11 | [config.json 内 uuids 和 paths 不对应](./Editor/faq.md#q11) | FAQ问答 |
| 12 | [2.2.2 判断非 resources 文件夹的资源是否属于公共资源](./Editor/faq.md#q12) | FAQ问答 |
| 13 | [分包下所有文件路径](./Editor/faq.md#q13) | FAQ问答 |

### Customize
| 编号 | 子项 | 备注 |
| :---: | :---: | :---: |
| 1 | [引擎自定义](./EngineCustomization) | 功能定制 |
| 2 | [脚本模板自定义](./ScriptTemplateCustomization) | 功能定制 |
| 3 | [范例模板自定义](./ExampleTemplateCustomization) | 功能定制 |