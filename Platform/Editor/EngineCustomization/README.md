## Cocos Creator FAQ

### 简介
自定义引擎

### 使用
- 可以选择从DashBoard上找到 编辑器, 选择需要自定义引擎的版本的编辑器，双击安装地址，进入到编辑器安装目录
    > macOS系统 如 /Applications/CocosCreator/Creator/3.0.0/CocosCreator.app
    >
    > Windows系统 如E:\CocosDashboard\resources\.editors\Creator\3.0.0\CocosCreator.exe

![image](../../pic/image/202206/2022063011.jpeg)
- 拷贝编辑器下的engine到自定义的文件路径下
    > macOS系统 如 /Applications/CocosCreator/Creator/3.0.0/CocosCreator.app/Contents/Resources/resources/3d/engine
    >
    > Windows系统 如 E:\CocosDashboard\resources\.editors\Creator\3.0.0\resources\resources\3d\engine
- 自定义文件路径 参考
    > macOS系统 如 /Users/apple/work/myCocosEngine/3.0.0/
    >
    > Window系统 如 D:\myCCEngine\3.0.0\
- 在命令终端，进入到自定义引擎engine的目录下 , 即 /Users/apple/work/myCocosEngine/3.0.0/engine目录下

![image](../../../pic/image/202206/2022063012.jpeg)
- 安装编译依赖
```ts
npm install -g gulp
```
```ts
npm install
```
- 修改自定义引擎engine下的内容,编译
```ts
gulp build
```
- 需要在编辑器上修改，使用自定义引擎。可以通过Cocos Creator -> 偏好设置 -> 引擎管理器选项，设置自定义引擎的路径为自定义文件路径，如/Users/apple/work/myCocosEngine/3.0.0/engine
- 保存(Ctrl+s 虽然是自动保存的，但是还是手动快捷键保存一下)，然后重启编辑器后生效