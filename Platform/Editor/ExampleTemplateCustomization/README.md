## Cocos Creator FAQ

### 简介
范例模板自定义

### 使用
- 找到引擎安装路径下
    > Windows系统  E:\CocosDashboard\resources\.editors\Creator\3.1.0\resources\templates
    > 
    > macOS系统  /Applications/CocosCreator/Creator/3.0.0/CocosCreator.app/Contents/Resources/templates
- 把需要每次需要新建的自定义Demo模板工程放到该路径下, 如

![image](../../pic/image/202206/2022063001.png)
- 修改list.json文件, 添加新的Demo模板工程,如
[
    {
        "name": "empty",
        "type": "local",
        "path": "./empty"
    },
    {
      "name": "Hello World",
      "type": "local",
      "path": "./hello-3d-world"
    },
    {
        "name": "Example Taxi Game",
        "type": "local",
        "path": "./taxi"
    },
    {
        "name": "3DExample",
        "type": "local",
        "path": "./3DExample"
    }
]
- 重启Dashboard,验证是否添加成功

![image](../../../pic/image/202206/2022063002.png)