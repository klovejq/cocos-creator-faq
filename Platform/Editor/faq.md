## Cocos Creator FAQ

### Editor / 编辑器

---
#### Q1
##### Question: 在属性面板如何定义二维数组?
##### Version: Cocos Creator 2.x && 3.x    
Answer:    

2.x版本
```ts
const {ccclass, property} = cc._decorator;

@ccclass('test')
export class test {
    @property({type: cc.String})
    t0: string[] = [];
    @property({type: cc.Float})
    t1: number[] = [];
    @property({type: cc.Node})
    t2: cc.Node[] = [];
}
@ccclass
export class Main extends cc.Component {
    @property(test)
    a: test[] = [];
}
```

3.x版本
```ts
import { _decorator, Component, CCFloat } from 'cc';
const { ccclass, property } = _decorator;

@ccclass('TestArray')
export class TestArray {
    @property({type: CCFloat })
    t0 : number[] = [];
}
@ccclass('Test')
export class Test extends Component {
    @property(TestArray)
    a : TestArray[] = [];
}
```

---
#### Q2
##### Question: CocosCreator 3.4.0 构建报错([Build]Cannot read property 'type' of undefined @getDependUuidsDeep xxx)
##### Version: Cocos Creator 3.4.0
Answer:    

构建的时候，项目中依赖的资源丢失导致的报错。

---
#### Q3
##### Question: Cocos Creator 2.x Setting 文件中的 packedAssets 的生成规则
##### Version: Cocos Creator 2.x
Answer:    

packedAssets 是用来描述合并后 JSON 的，key 值是合并后生成的 JSON 的文件名，value 值是被合并的资源的 key 值。

--- 
#### Q4
##### Question: 构建后资源压缩比例
##### Version: Cocos Creator All
Answer:    

特效图片可以提高压缩率（60至70），模型可以使用压缩率（70至80），UI 合图和字体合图可以使用高质量压缩（90）。 @link https://forum.cocos.org/t/topic/76554

---
#### Q5
##### Question: Cocos Creator 2.x 关闭资源刷新?
##### Version: Cocos Creator 2.x
Question Addtion:    

项目比较大的时候，刷新资源要耗费很长的事件，想要关闭资源的刷新。    

Answer:    

2.x 的项目，可以修改 Creator 本地缓存文件中的本地刷新配置来关闭资源刷新。如 Windows 系统，修改 C://Users/Administrator/.CocosCreator/profiles/settings.json 中的 "watch-file" 参数，修改为 false 就不会自动刷新资源。另外刷新资源，可以在插件中使用 Editor.assetdb.refresh 来刷新某个资源。

---
#### Q6
##### Question: 同一个项目构建不同的安卓包
##### Version: Cocos Creator 3.x
Question Addtion:    

需要在同一个项目，打包不同的安卓包因为我们可能需要根据不同的渠道对接不同的sdk，但是目前直接构建只有一个 native 文件夹，如何实现同一个项目构建不同的 native 文件夹呢?    

Answer:    

参考官方文档 https://docs.cocos.com/creator/manual/zh/editor/publish/custom-build-plugin.html 扩展构建流程, 文件的拷贝需要自行拓展，构建扩展包可以通过菜单栏 -> 扩展 -> 创建扩展 快速生成扩展空模板。

---
#### Q7
##### Question: Dashboard 导入 2.0.10 项目失败
##### Version: Cocos Creator 2.0.10 && <= 2.3.2
Answer:    

修改项目下的project.json
```
{
    "engine": "cocos-creator-js",
    "type": "2d",
    "version": "2.0.10",
    "packages": "packages"
}
```

---
#### Q8
##### Question: 压缩纹理配置里面的百分数的含义
##### Version: Cocos Creator 3.0
Question Addtion:    

压缩纹理配置里面的百分数的含义， 如果配置 JPG 和 PNG 则显示异常(发黑)，单独配置 PNG 则显示正常

Answer:    

压缩纹理配置里面的百分数代表压缩到圆度的百分比。如果同时配置 JPG 和 PNG，则优先加载的是 JPG，而不是 PNG。
```
[压缩纹理详解](https://docs.cocos.com/creator/manual/zh/asset/compress-texture.html#%E5%8E%8B%E7%BC%A9%E7%BA%B9%E7%90%86%E8%AF%A6%E8%A7%A3)
Cocos Creator 3.0 在构建图片的时候，会查找当前图片是否进行了压缩纹理的配置，如果没有，则最后按原图输出。

如果查找到了压缩纹理的配置，那么会按照找到的配置对图片进行纹理压缩。项目设置里压缩纹理配置是按照平台大类划分的，具体到实际平台的支持程度会有一些差异。构建将会根据 实际构建平台 以及当前 图片纹理的透明通道 情况来对配置的纹理格式做一定的剔除和优先级选择，关于这块规则可以参考下文的示例来理解。

这些生成的图片不会都被加载到引擎中，引擎会根据 macro.SUPPORT_TEXTURE_FORMATS 中的配置来选择加载合适格式的图片。macro.SUPPORT_TEXTURE_FORMATS 列举了当前平台支持的所有图片格式，引擎加载图片时会从生成的图片中找到在这个列表中 优先级靠前（即排列靠前）的格式来加载。

开发者可以通过修改 macro.SUPPORT_TEXTURE_FORMATS 来自定义平台的图片资源支持情况以及加载顺序的优先级。
```
而从 [macro.SUPPORT_TEXTURE_FORMATS](https://github.com/cocos-creator/engine/blob/v3.4.0/cocos/core/platform/macro.ts#L35) 所有平台支持的格式列表 ['.webp', '.jpg', '.jpeg', '.bmp', '.png'] 可以看出，如果同时配置JPG和PNG，JPG是优先被选择作为加载的格式的。
```
const SUPPORT_TEXTURE_FORMATS = ['.astc', '.pkm', '.pvr', '.webp', '.jpg', '.jpeg', '.bmp', '.png'];
```

---
#### Q9
##### Question: Cocos Creator 3.x 图片无法拖到层级管理器上显示?
##### Version: Cocos Creator 3.x
Answer:    

点击资源管理器中的图片，选择图片的 TYPE 为 sprite-frame。

---
#### Q10
##### Question: 如何快速定位模块中未勾选但已在项目中使用的模块?
##### Version: Cocos Creator 3.x
Answer:    

目前未勾选但是被剔除的功能模块，在运行时会提示 xxx is undefined 报错。另外，可以通过构建 web 版本后的 cc.js 内容来分辨引擎模块(这个你需要比较熟悉哪些模块对应哪些代码) 或者 settings/project.json/excluded-modules 来区分你的模块。

---
#### Q11
##### Question: config.json 内的 uuids 和 paths 不对应?
Question Addtion:    

Cocos Creator 2.4.6, 需要对处理 debug 和 release 模式的功能限制。发现 config.json 中，一些 uuid 在 path 中找不到。而且 release 模式下 path 的 key 值也可能不相邻。    
##### Version: Cocos Creator 2.4.6
Answer:    

path 的列表里是所有资源的路径，但是这个资源可能包括 texture、spriteFrame 等，然后有些 uuid 存在于一个比较大的资源下，比如资源挂载到场景下，这个时候，需要在场景资源里面才能找到该 uuid 的资源。换句话说，path 是所有文件的信息，uuid 包括了精灵帧等，所以会对应不连续，会比 path 的条目多。

---
#### Q12
##### Question: Cocos Creator 2.2.2 怎么判断非 resources 文件夹的资源是否属于公共资源？
##### Version: Cocos Creator 2.2.2
Answer:    

目前引擎（2.2.2）没有办法在运行时将 res 的包体资源相对路径转化为项目相对路径。可以参考：
  - 方式1 通过监听 cc.loader.onProgress = function (completedCount, totalCount, item) {}; 可以获取到每次加载的实例。或者在加载完成之后通过 cc.loader.getDependsRecursively("resUrl") 获取实例。取得实例之后缓存他们的路径，然后可以通过 cc.loader.getRes(url, cc.Asset) 获取到原始资源对象。此时就可以通过解读原始对象数据，判断是不是公用资源。
  - 方式2 通过插件获取项目发布之后的资源路径，然后将该路径数据整理出来，记录成 json，放到项目中。然后你每次需要卸载资源的时候，通过第一步获取到的路径，然后去比对，如果路径已经被记录在公有资源 json 中，那么就可以卸载。
  - 参考源码
  > CCLoader 源码 https://github.com/cocos-creator/engine/blob/9b7a7dc11ce49f0fdca3c34df5ab59604060c0a4/cocos2d/core/load-pipeline/CCLoader.js#L102
  >
  > CCLoader API https://docs.cocos.com/creator/api/zh/classes/loader.html#assetloader
  >
  > 项目导出后的路径可以通过这个插件来查看 https://forum.cocos.org/t/topic/76554
  >
  > 官方插件 https://github.com/cocos-creator/demo-process-build-textures

---
#### Q13
#### Question: Cocos Creator 有没有获取分包下所有文件路径的方法?
##### Version: Cocos Creator All
Answer:    

目前引擎没有提供这种方法，可以自己封装。获取 Bundle 之后，获取 config/paths 下的 _map 数组。