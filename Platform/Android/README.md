## Cocos Creator FAQ
### Android - 安卓

### Usage
| 编号 | 子项 | 备注 |
| :---: | :---: | :---: |
| 1 | [AndroidStudio ADB 连接 MuMu 模拟器](./usage.md#1) | 基础使用 |
| 2 | [AndroidStudio ADB 连接夜神模拟器](./usage.md#2) | 基础使用 |
| 3 | [Android 利用 addr2line 工具定位奔溃行数](./usage.md#3) | 基础使用 |
| 4 | [Android 安装 release 包](./usage.md#4) | 基础使用 |

### FAQ
| 编号 | 子项 | 备注 |
| :---: | :---: | :---: |
| 1 | [Bugly 捕获奔溃异常分析'E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method "java.lang.String de.a()" on a null object reference'](./faq.md#q1) | FAQ问答 |
| 2 | [下载 apk](./faq.md#q2) | FAQ问答 |
| 3 | [打包安卓后透明材质无法显示](./faq.md#q3) | FAQ问答 |