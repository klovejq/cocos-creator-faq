## Cocos Creator FAQ

### Android - 安卓

---
#### Q1
##### Question: bugly捕获奔溃异常 'E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method "java.lang.String de.a()" on a null object reference'
- BUG 信息: E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method "java.lang.String de.a()" on a null object reference      
- Detail 信息:
```
 ... 
W System : ClassLoader reference unknown path : system/framework/mediatek-cta.jar
 ...
D AndroidRuntime: Shutting down VM
-------- beginning of crash
E AndroidRuntime : FATAL EXCEPTION : main
...
E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.String de.a()' on a null object reference
...
E AndroidRuntime : at com.http.lib.request.Request$c$g.run(Request.java:1)
...
```
```
 ... 
W System : ClassLoader reference unknown path : system/framework/mediatek-cta.jar
 ...
D OpenSSLLib: OpensslErr:Module:12(177:); file:External/boringssl/src/crypto/asn1/asn1_lib.c; Line: 168; Function: ASN1_get_object
 ...
D AndroidRuntime: Shutting down VM
-------- beginning of crash
E AndroidRuntime : FATAL EXCEPTION : main
...
E AndroidRuntime: java.lang.NullPointerException: Attempt to invoke virtual method 'java.lang.String de.a()' on a null object reference
...
E AndroidRuntime : at com.http.lib.request.Request$c$g.run(Request.java:1)
...
```
Question Addtion:    

在 bugly 奔溃检测上以上信息，看似刚启动就奔溃了，是否是已知问题，而且不是必现问题。
##### Version: Creator 2.4.6 
Answer:    

- 不是已知问题，没有收到类似反馈。前面看到有 jar 包丢失了，排查是不是 dex 分包导致的，有没有使用到 dex 分包，或者剔除了相关的东西。另外如果构建 API 为30或者31的之前有类似反馈，会出问题，可以考虑降低API看看 https://forum.cocos.org/t/topic/122966

---
### Q2
##### Question: 下载apk如何操作?
Question Addtion:    

需要从远程CDN服务器下载apk包并安装打开.    
##### Version: Creator All
Answer:    

引擎目前没有提供".apk"格式的文件下载, 可以自行扩展。另外如果远程CDN服务器上下载的apk包过大，可能会导致使用assetManager.loadAny下载接口下载超时，可以修改CocosDownloader.java脚本中的OkHttpClient.class中createDownloader接口中的 downloader._httpClient 的 callTimeout改为pingInterval 模式。另外可以通过java层下载apk，可以参考 https://blog.csdn.net/ly_xiamu/article/details/83089534。

---
#### Q3
##### Question: 打包安卓后透明材质无法显示?
Question Addtion:    

Cocos Creator 3.4.0 版本打包安卓后，透明材质无法显示。    
##### Version: Creator 3.4.0
Answer:    

打包安卓，是支持透明材质的，透明物体是按照顺序渲染的，透明材质无法显示的问题，怀疑是被遮挡了。检查一下你这个材质是否勾选了深度检测。

---
#### Q4
##### Question: 热更新数据太大的时候会报错，重启继续更新就可以正常更新。 
Question Addtion:    

热更新大了的时候基本是必现的。
##### Version: Creator 2.4.6   
Answer:    

